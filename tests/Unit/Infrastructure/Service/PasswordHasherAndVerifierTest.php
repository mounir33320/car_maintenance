<?php

namespace Tests\Unit\Infrastructure\Service;

use Infrastructure\Service\PasswordHasherAndVerifier;
use PHPUnit\Framework\TestCase;

class PasswordHasherAndVerifierTest extends TestCase
{
    private PasswordHasherAndVerifier $passwordHasherAndVerifier;

    public function testHashAndVerifyPassword(): void
    {
        $plainPassword = 'password';
        $hashedPassword = $this->passwordHasherAndVerifier->hash($plainPassword);
        $this->assertTrue($this->passwordHasherAndVerifier->verify($plainPassword, $hashedPassword));
    }

    public function testReturnFalseIfPasswordIsNotValid(): void
    {
        $plainPassword = 'password';
        $hashedPassword = $this->passwordHasherAndVerifier->hash($plainPassword);
        $invalidPassword = 'invalid_password';
        $this->assertFalse($this->passwordHasherAndVerifier->verify($invalidPassword, $hashedPassword));
    }

    protected function setUp(): void
    {
        $this->passwordHasherAndVerifier = new PasswordHasherAndVerifier();
    }
}
