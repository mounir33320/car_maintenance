<?php

namespace Tests\Unit\Application\User\SignIn;

use Application\Service\PasswordHasherInterface;
use Application\Service\PasswordVerifierInterface;
use Application\Shared\Error\ErrorNotification;
use Application\User\Shared\UserErrorMessageConstants;
use Application\User\SignIn\SignIn;
use Application\User\SignIn\SignInInterface;
use Application\User\SignIn\SignInPresenterInterface;
use Application\User\SignIn\SignInRequest;
use Application\User\SignIn\SignInResponse;
use Domain\User\Entity\User;
use Domain\User\Repository\UserRepositoryInterface;
use Domain\User\RoleEnum;
use Infrastructure\Service\PasswordHasherAndVerifier;
use PHPUnit\Framework\TestCase;
use Tests\_Mock\Repository\User\UserInMemoryRepository;

class SignInTest extends TestCase implements SignInPresenterInterface
{
    private UserRepositoryInterface $userRepository;
    private PasswordVerifierInterface $passwordVerifier;
    private PasswordHasherInterface $passwordHasher;
    private SignInResponse $response;
    private SignInInterface $signIn;

    private const VALID_PASSWORD = 'validPassword0898@';

    public function present(SignInResponse $response): void
    {
        $this->response = $response;
    }

    public function testUserNotFound(): void
    {
        $request = new SignInRequest('not-found@test.fr', self::VALID_PASSWORD);

        $this->signIn->execute($request, $this);

        $expectedErrorNotif = (new ErrorNotification())->add('password', UserErrorMessageConstants::INVALID_CREDENTIALS);

        $this->assertEquals($expectedErrorNotif, $this->response->getErrorNotification());
    }

    public function testInvalidPassword(): void
    {
        $user = new User('user-id', 'test@test.fr', $this->passwordHasher->hash(self::VALID_PASSWORD), RoleEnum::RoleUser);
        $this->userRepository->createUser($user);

        $request = new SignInRequest($user->getEmail(), 'invalid-password');

        $this->signIn->execute($request, $this);

        $expectedErrorNotif = (new ErrorNotification())->add('password', UserErrorMessageConstants::INVALID_CREDENTIALS);

        $this->assertEquals($expectedErrorNotif, $this->response->getErrorNotification());
    }

    public function testSignIn(): void
    {
        $user = new User('user-id', 'test@test.fr', $this->passwordHasher->hash(self::VALID_PASSWORD), RoleEnum::RoleUser);
        $this->userRepository->createUser($user);

        $request = new SignInRequest($user->getEmail(), self::VALID_PASSWORD);

        $this->signIn->execute($request, $this);

        $this->assertEquals($user, $this->response->getUser());
    }

    protected function setUp(): void
    {
        $this->userRepository = new UserInMemoryRepository();
        $this->passwordVerifier = new PasswordHasherAndVerifier();
        $this->passwordHasher = new PasswordHasherAndVerifier();

        $this->signIn = new SignIn($this->userRepository, $this->passwordVerifier);
    }
}
