<?php

namespace Tests\Unit\Application\User\SignUp;

use Application\Service\PasswordFormatChecker;
use Application\Shared\Error\Error;
use Application\Shared\Error\ErrorNotification;
use Application\User\Shared\UserErrorMessageConstants;
use Application\User\SignUp\SignUp;
use Application\User\SignUp\SignUpInterface;
use Application\User\SignUp\SignUpPresenterInterface;
use Application\User\SignUp\SignUpRequest;
use Application\User\SignUp\SignUpResponse;
use Domain\User\Entity\User;
use Domain\User\Repository\UserRepositoryInterface;
use Domain\User\RoleEnum;
use Infrastructure\Service\PasswordHasherAndVerifier;
use Infrastructure\Service\UUIDGenerator;
use PHPUnit\Framework\TestCase;
use Tests\_Mock\Repository\User\UserInMemoryRepository;

class SignUpTest extends TestCase implements SignUpPresenterInterface
{
    private const VALID_PASSWORD = '123Tinaturner!';
    private const ROLE_USER = 'user';

    private const EMAIL = 'test@test.fr';
    private SignUpResponse $response;
    private UserRepositoryInterface $userRepository;
    private SignUpInterface $createUser;

    public function present(SignUpResponse $response): void
    {
        $this->response = $response;
    }

    public function testInvalidFormatEmail(): void
    {
        $request = new SignUpRequest(
            'invalid_format',
            self::VALID_PASSWORD,
            self::ROLE_USER
        );

        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'email',
            UserErrorMessageConstants::INVALID_EMAIL_FORMAT
        );

        self::assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testInvalidPassword(): void
    {
        $request = new SignUpRequest(
            self::EMAIL,
            'invalid-password',
            self::ROLE_USER
        );
        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'password',
            UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER
        );

        self::assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testInvalidPasswordWithUpperCaseLetterMissing(): void
    {
        $request = new SignUpRequest(
            self::EMAIL,
            '123azerty!',
            self::ROLE_USER
        );
        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'password',
            UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER
        );

        self::assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testInvalidPasswordWithLowerCaseLetterMissing(): void
    {
        $request = new SignUpRequest(
            self::EMAIL,
            '123AZERTY!',
            self::ROLE_USER
        );

        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'password',
            UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER
        );

        self::assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testInvalidPasswordWithSpecialCharacterMissing(): void
    {
        $request = new SignUpRequest(
            self::EMAIL,
            '123AZERTYytreza',
            self::ROLE_USER
        );

        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'password',
            UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER
        );

        self::assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testInvalidPasswordWithLengthLessThan6Characters(): void
    {
        $request = new SignUpRequest(
            self::EMAIL,
            'Az6!',
            self::ROLE_USER
        );

        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'password',
            UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER
        );

        self::assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testValidPassword(): void
    {
        $request = new SignUpRequest(
            self::EMAIL,
            self::VALID_PASSWORD,
            self::ROLE_USER
        );

        $this->createUser->execute($request, $this);

        $errors = array_filter(
            $this->response->getErrorNotification()->getErrors(),
            fn (Error $error) => 'password' === $error->getFieldName()
        );

        self::assertEmpty($errors);
    }

    public function testExistingUser(): void
    {
        $existingUser = new User(
            'id',
            self::EMAIL,
            self::VALID_PASSWORD,
            RoleEnum::RoleUser
        );

        $this->userRepository->createUser($existingUser);

        $request = new SignUpRequest(
            self::EMAIL,
            self::VALID_PASSWORD,
            'user'
        );

        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('email', UserErrorMessageConstants::USER_ALREADY_EXISTS);

        $this->assertTrue($this->response->getErrorNotification()->hasError());
        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testInvalidRole(): void
    {
        $request = new SignUpRequest(self::EMAIL, self::VALID_PASSWORD, 'invalid');
        $this->createUser->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('role', UserErrorMessageConstants::INVALID_ROLE);

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testResponseReturnCreatedUser(): void
    {
        $request = new SignUpRequest(self::EMAIL, self::VALID_PASSWORD, 'user');
        $this->createUser->execute($request, $this);

        $createdUser = $this->response->getCreatedUser();
        $this->assertInstanceOf(User::class, $createdUser);
    }

    public function testCreatedUserWithEmailAndRoleAreSame(): void
    {
        $request = new SignUpRequest(self::EMAIL, self::VALID_PASSWORD, 'user');
        $this->createUser->execute($request, $this);

        $createdUser = $this->response->getCreatedUser();

        $this->assertNotNull($createdUser);
        $this->assertSame(self::EMAIL, $createdUser->getEmail());
        $this->assertSame(RoleEnum::RoleUser, $createdUser->getRole());
    }

    protected function setUp(): void
    {
        $this->userRepository = new UserInMemoryRepository();
        $passwordHasher = new PasswordHasherAndVerifier();
        $passwordFormatChecker = new PasswordFormatChecker();

        $this->createUser = new SignUp($this->userRepository, $passwordHasher, new UUIDGenerator(), $passwordFormatChecker);
    }
}
