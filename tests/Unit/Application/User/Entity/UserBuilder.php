<?php

namespace Tests\Unit\Application\User\Entity;

use Domain\User\Entity\User;
use Domain\User\RoleEnum;

final class UserBuilder
{
    public const ID = 'id';
    public const EMAIL = 'test@test.fr';
    public const PASSWORD = 'password';

    private function __construct()
    {
    }

    public static function build(
        string $id = self::ID,
        string $email = self::EMAIL,
        string $password = self::PASSWORD,
        RoleEnum $role = RoleEnum::RoleUser,
        bool $isActive = true
    ): User {
        return new User($id, $email, $password, $role, $isActive);
    }
}
