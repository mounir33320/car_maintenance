<?php

namespace Tests\Unit\Application\User\UpdatePassword;

use Application\Service\PasswordFormatChecker;
use Application\Shared\Error\ErrorNotification;
use Application\User\Shared\UserErrorMessageConstants;
use Application\User\UpdatePassword\UpdatePassword;
use Application\User\UpdatePassword\UpdatePasswordInterface;
use Application\User\UpdatePassword\UpdatePasswordPresenterInterface;
use Application\User\UpdatePassword\UpdatePasswordRequest;
use Application\User\UpdatePassword\UpdatePasswordResponse;
use Domain\User\Entity\User;
use Domain\User\Repository\UserRepositoryInterface;
use Infrastructure\Service\PasswordHasherAndVerifier;
use PHPUnit\Framework\TestCase;
use Tests\_Mock\Repository\User\UserInMemoryRepository;
use Tests\Unit\Application\User\Entity\UserBuilder;

class UpdatePasswordTest extends TestCase implements UpdatePasswordPresenterInterface
{
    private const VALID_PASSWORD = '123Tinaturner!';

    private UserRepositoryInterface $userRepository;
    private UpdatePasswordInterface $updateUserPassword;
    private UpdatePasswordResponse $response;
    private PasswordHasherAndVerifier $passwordHasherAndVerifier;

    public function present(UpdatePasswordResponse $response): void
    {
        $this->response = $response;
    }

    public function testNotFoundUser(): void
    {
        $request = new UpdatePasswordRequest(
            'current-user-id',
            'not-existing-id',
            'oldPlainPassword',
            'newPlainPassword'
        );

        $this->updateUserPassword->execute($request, $this);
        $expectedErrorNotif = (new ErrorNotification())->add('id', UserErrorMessageConstants::USER_NOT_FOUND);
        $this->assertEquals($expectedErrorNotif, $this->response->getErrorNotification());
    }

    public function testUpdateOwnUserPassword(): void
    {
        $currentUser = UserBuilder::build();
        $this->userRepository->createUser($currentUser);

        $userToUpdate = UserBuilder::build('another-id');
        $this->userRepository->createUser($userToUpdate);

        $request = new UpdatePasswordRequest(
            $currentUser->getId(),
            $userToUpdate->getId(),
            'oldPlainPassword',
            'newPlainPassword'
        );

        $this->updateUserPassword->execute($request, $this);

        $expectedErrorNotif = (new ErrorNotification())->add('id', UserErrorMessageConstants::USER_CANNOT_UPDATE_ANOTHER_USER);
        $this->assertEquals($expectedErrorNotif, $this->response->getErrorNotification());
    }

    public function testInvalidNewPassword(): void
    {
        $user = UserBuilder::build();
        $this->userRepository->createUser($user);

        $request = new UpdatePasswordRequest($user->getId(), $user->getId(), self::VALID_PASSWORD, 'invalid-password');
        $this->updateUserPassword->execute($request, $this);
        $expectedErrorNotif = (new ErrorNotification())->add('password', UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER);

        $this->assertEquals($expectedErrorNotif, $this->response->getErrorNotification());
    }

    public function testWrongOldPassword(): void
    {
        $user = UserBuilder::build(password: self::VALID_PASSWORD);
        $this->userRepository->createUser($user);

        $request = new UpdatePasswordRequest($user->getId(), $user->getId(), 'wrong-password', self::VALID_PASSWORD);
        $this->updateUserPassword->execute($request, $this);

        $expectedErrorNotif = (new ErrorNotification())->add('password', UserErrorMessageConstants::WRONG_PASSWORD);
        $this->assertEquals($expectedErrorNotif, $this->response->getErrorNotification());
    }

    public function testUserPasswordUpdated(): void
    {
        $oldHashedPassword = $this->passwordHasherAndVerifier->hash(self::VALID_PASSWORD);
        $user = UserBuilder::build(password: $oldHashedPassword);
        $this->userRepository->createUser($user);

        $newPassword = '1234MarioKart@';

        $request = new UpdatePasswordRequest($user->getId(), $user->getId(), self::VALID_PASSWORD, $newPassword);
        $this->updateUserPassword->execute($request, $this);

        $this->assertInstanceOf(User::class, $this->response->getUpdatedUser());
        $this->assertTrue($this->passwordHasherAndVerifier->verify($newPassword, $this->response->getUpdatedUser()->getPassword()));

        $fetchedUser = $this->userRepository->findUserById($user->getId());
        $this->assertInstanceOf(User::class, $fetchedUser);
        $this->assertTrue($this->passwordHasherAndVerifier->verify($newPassword, $fetchedUser->getPassword()));
    }

    protected function setUp(): void
    {
        $this->userRepository = new UserInMemoryRepository();
        $this->passwordHasherAndVerifier = new PasswordHasherAndVerifier();
        $passwordFormatChecker = new PasswordFormatChecker();

        $this->updateUserPassword = new UpdatePassword(
            $this->userRepository,
            $this->passwordHasherAndVerifier,
            $this->passwordHasherAndVerifier,
            $passwordFormatChecker
        );
    }
}
