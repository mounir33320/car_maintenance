<?php

namespace Tests\Unit\Application\Vehicle\RetrieveSingleVehicle;

use Application\Shared\Error\ErrorNotification;
use Application\Vehicle\RetrieveSingleVehicle\RetrieveSingleVehicle;
use Application\Vehicle\RetrieveSingleVehicle\RetrieveSingleVehiclePresenterInterface;
use Application\Vehicle\RetrieveSingleVehicle\RetrieveSingleVehicleRequest;
use Application\Vehicle\RetrieveSingleVehicle\RetrieveSingleVehicleResponse;
use Application\Vehicle\Shared\VehicleErrorMessageConstants;
use Domain\Vehicle\Entity\Vehicle;
use Domain\Vehicle\Repository\VehicleRepositoryInterface;
use PHPUnit\Framework\TestCase;
use Tests\_Mock\Repository\Vehicle\VehicleInMemoryRepository;

class RetrieveSingleVehicleTest extends TestCase implements RetrieveSingleVehiclePresenterInterface
{
    private RetrieveSingleVehicleResponse $response;
    private VehicleRepositoryInterface $vehicleRepository;
    private RetrieveSingleVehicle $retrieveSingleVehicle;

    private const VEHICLE_ID = 'vehicle_id';
    private const USER_ID = 'user_id';
    private const MAKE = 'Nissan';
    private const MODEL = 'Interstar';
    private const REGISTRATION_DATE = '2010-05-05';

    public function present(RetrieveSingleVehicleResponse $response): void
    {
        $this->response = $response;
    }

    public function testNotFoundVehicle(): void
    {
        $request = new RetrieveSingleVehicleRequest(self::USER_ID, 'wrong_vehicle_id');
        $this->retrieveSingleVehicle->execute($request, $this);

        $expectedErrorNotification = (new ErrorNotification())->add(
            'id',
            VehicleErrorMessageConstants::VEHICLE_NOT_FOUND
        );

        $this->assertEquals($expectedErrorNotification, $this->response->getErrorNotification());
    }

    public function testNotFoundIfCurrentUserIdDifferentVehicleUserId(): void
    {
        $vehicle = new Vehicle(
            self::VEHICLE_ID,
            self::USER_ID,
            self::MAKE,
            self::MODEL,
            new \DateTimeImmutable(self::REGISTRATION_DATE)
        );

        $this->vehicleRepository->create($vehicle);

        $request = new RetrieveSingleVehicleRequest('wrong_user_id', self::VEHICLE_ID);
        $this->retrieveSingleVehicle->execute($request, $this);

        $expectedErrorNotification = (new ErrorNotification())->add(
            'id',
            VehicleErrorMessageConstants::VEHICLE_NOT_FOUND
        );

        $this->assertEquals($expectedErrorNotification, $this->response->getErrorNotification());
    }

    protected function setUp(): void
    {
        $this->vehicleRepository = new VehicleInMemoryRepository();
        $this->retrieveSingleVehicle = new RetrieveSingleVehicle($this->vehicleRepository);
    }
}
