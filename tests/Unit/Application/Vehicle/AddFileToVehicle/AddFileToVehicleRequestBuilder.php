<?php

namespace Tests\Unit\Application\Vehicle\AddFileToVehicle;

use Application\Vehicle\AddFileToVehicle\AddFileToVehicleRequest;

class AddFileToVehicleRequestBuilder
{
    private string $vehicleId;
    private string $userId;
    private string $originalName;
    private string $filePath;
    private string $type;
    private int $fileSize;
    private string $mimeType;

    public function withVehicleId(string $vehicleId): AddFileToVehicleRequestBuilder
    {
        $this->vehicleId = $vehicleId;

        return $this;
    }

    public function withUserId(string $userId): AddFileToVehicleRequestBuilder
    {
        $this->userId = $userId;

        return $this;
    }

    public function withOriginalName(string $originalName): AddFileToVehicleRequestBuilder
    {
        $this->originalName = $originalName;

        return $this;
    }

    public function withFilePath(string $filePath): AddFileToVehicleRequestBuilder
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function withType(string $type): AddFileToVehicleRequestBuilder
    {
        $this->type = $type;

        return $this;
    }

    public function withFileSize(int $fileSize): AddFileToVehicleRequestBuilder
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    public function withMimeType(string $mimeType): AddFileToVehicleRequestBuilder
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function build(): AddFileToVehicleRequest
    {
        return new AddFileToVehicleRequest(
            $this->vehicleId,
            $this->userId,
            $this->originalName,
            $this->filePath,
            $this->type,
            $this->fileSize,
            $this->mimeType
        );
    }
}
