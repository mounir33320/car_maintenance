<?php

namespace Tests\Unit\Application\Vehicle\AddFileToVehicle;

use Application\Shared\Error\ErrorNotification;
use Application\User\Shared\UserErrorMessageConstants;
use Application\Vehicle\AddFileToVehicle\AddFileToVehicle;
use Application\Vehicle\AddFileToVehicle\AddFileToVehicleInterface;
use Application\Vehicle\AddFileToVehicle\AddFileToVehiclePresenterInterface;
use Application\Vehicle\AddFileToVehicle\AddFileToVehicleRequest;
use Application\Vehicle\AddFileToVehicle\AddFileToVehicleResponse;
use Application\Vehicle\Shared\VehicleErrorMessageConstants;
use Application\Vehicle\Shared\VehicleFileErrorMessageConstants;
use Domain\User\Entity\User;
use Domain\User\Repository\UserRepositoryInterface;
use Domain\User\RoleEnum;
use Domain\Vehicle\Entity\Vehicle;
use Domain\Vehicle\Repository\VehicleFileStorageInterface;
use Domain\Vehicle\VehicleFileType;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use Tests\_Mock\Repository\User\UserInMemoryRepository;
use Tests\_Mock\Repository\Vehicle\VehicleInMemoryRepository;

class AddFileToVehicleTest extends TestCase implements AddFileToVehiclePresenterInterface
{
    private const ORIGINAL_NAME = 'originalName.pdf';
    private const FILE_PATH = 'path/to/file/originalName.pdf';
    private const FILE_SIZE = 5 * 1024 * 1024;
    private const INSURANCE_TYPE = VehicleFileType::INSURANCE->value;

    private const PDF_MIME_TYPE = 'application/pdf';

    private const USER_ID = 'user-id';
    private const VEHICLE_ID = 'vehicle-id';
    private const MAKE = 'Nissan';
    private const MODEL = 'Interstar';
    private const FIRST_REGISTRATION_DATE = '2010-05-06';
    private const REGISTRATION_NUMBER = 'AR-659-RV';
    private const PURCHASE_PRICE = 700000;
    private const MILEAGE_AT_PURCHASE = 220000;
    private const CURRENT_KNOWN_MILEAGE = 220000;
    private const LAST_OIL_CHANGE_DATE = '2024-04-04';
    private const LAST_OIL_CHANGE_MILEAGE = 222000;
    private const TIMING_BELT_SERVICE_INTERVALS_IN_YEAR = 5;
    private const TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE = 120000;
    private const LAST_TIMING_BELT_CHANGE_MILEAGE = 210000;
    private const LAST_TIMING_BELT_CHANGE_DATE = '2022-06-06';
    private const LAST_TECHNICAL_INSPECTION_DATE = '2024-01-18';
    private const LAST_POLLUTION_CONTROL_DATE = '2023-06-12';
    private AddFileToVehicleResponse $response;
    private AddFileToVehicleInterface $addFileToVehicle;
    private VehicleInMemoryRepository $vehicleRepository;
    private UserRepositoryInterface $userRepository;

    public function present(AddFileToVehicleResponse $response): void
    {
        $this->response = $response;
    }

    public function testUserNotFound(): void
    {
        $this->createVehicle();
        $request = new AddFileToVehicleRequest(
            self::VEHICLE_ID,
            'user-not-found',
            self::ORIGINAL_NAME,
            self::FILE_PATH,
            self::INSURANCE_TYPE,
            self::FILE_SIZE,
            self::PDF_MIME_TYPE
        );

        $this->addFileToVehicle->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('userId', UserErrorMessageConstants::USER_NOT_FOUND);

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testVehicleNotFound(): void
    {
        $this->createUser();
        $request = new AddFileToVehicleRequest(
            'vehicle-id-not-found',
            self::USER_ID,
            self::ORIGINAL_NAME,
            self::FILE_PATH,
            self::INSURANCE_TYPE,
            self::FILE_SIZE,
            self::PDF_MIME_TYPE
        );

        $this->addFileToVehicle->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'vehicleId',
            VehicleErrorMessageConstants::VEHICLE_NOT_FOUND
        );

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    #[DataProvider('vehicleFileValuesProvider')]
    public function testRequestValidation(
        AddFileToVehicleRequest $request,
        bool $hasError,
        ?string $field = null,
        ?string $message = null
    ): void {
        $this->createUser();
        $this->createVehicle();

        $this->addFileToVehicle->execute($request, $this);

        if ($hasError) {
            self::assertNotNull($field);
            self::assertNotNull($message);
            $expectedErrorNotif = (new ErrorNotification())->add($field, $message);

            $this->assertEquals($expectedErrorNotif, $this->response->getErrorNotification());
        } else {
            $vehicles = $this->vehicleRepository->findByUserId(self::USER_ID);
            $this->assertEquals($vehicles[0]->insuranceFiles()[0], $this->response->getFile());
        }
    }

    /**
     * @return array<string, array{0: AddFileToVehicleRequest, 1: bool, 2?: string, 3?: string}>
     */
    public static function vehicleFileValuesProvider(): array
    {
        $requestBuilder = (new AddFileToVehicleRequestBuilder())
            ->withVehicleId(self::VEHICLE_ID)
            ->withUserId(self::USER_ID)
            ->withFilePath(self::FILE_PATH)
            ->withFileSize(self::FILE_SIZE)
            ->withType(self::INSURANCE_TYPE)
            ->withFileSize(self::FILE_SIZE)
            ->withOriginalName(self::ORIGINAL_NAME)
            ->withMimeType(self::PDF_MIME_TYPE)
        ;

        return [
            'Too large file' => [
                $requestBuilder->withFileSize(15 * 1024 * 1024)->build(),
                true,
                'fileSize',
                VehicleFileErrorMessageConstants::TOO_LARGE_FILE
            ],
            'Incorrect file type' => [
                $requestBuilder
                    ->withFileSize(self::FILE_SIZE)
                    ->withType('incorrect-type')
                    ->build(),
                true,
                'vehicleFileType',
                VehicleFileErrorMessageConstants::INCORRECT_VEHICLE_FILE_TYPE
            ],
            'Incorrect file extension' => [
                $requestBuilder
                    ->withType(self::INSURANCE_TYPE)
                    ->withOriginalName('IncorrectExtension.doc')
                    ->build(),
                true,
                'fileExtension',
                VehicleFileErrorMessageConstants::INCORRECT_FILE_EXTENSION
            ],
            'Incorrect mime type' => [
                $requestBuilder
                    ->withOriginalName(self::ORIGINAL_NAME)
                    ->withMimeType('application/msword')
                    ->build(),
                true,
                'mimeType',
                VehicleFileErrorMessageConstants::INCORRECT_MIME_TYPE
            ],
            'Valid Request' => [
                $requestBuilder
                    ->withMimeType(self::PDF_MIME_TYPE)
                    ->build(),
                false
            ],
        ];
    }

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        $this->vehicleRepository = new VehicleInMemoryRepository();
        $this->userRepository = new UserInMemoryRepository();
        $fileStorage = $this->createMock(VehicleFileStorageInterface::class);

        $this->addFileToVehicle = new AddFileToVehicle($this->vehicleRepository, $fileStorage, $this->userRepository);
    }

    private function createUser(): void
    {
        $user = new User(self::USER_ID, 'test@test.fr', 'password', RoleEnum::RoleUser);

        $this->userRepository->createUser($user);
    }

    private function createVehicle(): void
    {
        $vehicle = new Vehicle(
            self::VEHICLE_ID,
            self::USER_ID,
            self::MAKE,
            self::MODEL,
            new \DateTimeImmutable(self::FIRST_REGISTRATION_DATE),
            self::REGISTRATION_NUMBER,
            self::PURCHASE_PRICE,
            self::MILEAGE_AT_PURCHASE,
            self::CURRENT_KNOWN_MILEAGE,
            new \DateTimeImmutable(self::LAST_OIL_CHANGE_DATE),
            self::LAST_OIL_CHANGE_MILEAGE,
            self::TIMING_BELT_SERVICE_INTERVALS_IN_YEAR,
            self::TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE,
            new \DateTimeImmutable(self::LAST_TIMING_BELT_CHANGE_DATE),
            self::LAST_TIMING_BELT_CHANGE_MILEAGE,
            new \DateTimeImmutable(self::LAST_TECHNICAL_INSPECTION_DATE),
            new \DateTimeImmutable(self::LAST_POLLUTION_CONTROL_DATE),
        );

        $this->vehicleRepository->create($vehicle);
    }
}
