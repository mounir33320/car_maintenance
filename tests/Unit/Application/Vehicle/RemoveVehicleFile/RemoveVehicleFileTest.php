<?php

namespace Tests\Unit\Application\Vehicle\RemoveVehicleFile;

use Application\Shared\Error\ErrorNotification;
use Application\Vehicle\RemoveVehicleFile\RemoveVehicleFile;
use Application\Vehicle\RemoveVehicleFile\RemoveVehicleFileInterface;
use Application\Vehicle\RemoveVehicleFile\RemoveVehicleFilePresenterInterface;
use Application\Vehicle\RemoveVehicleFile\RemoveVehicleFileRequest;
use Application\Vehicle\RemoveVehicleFile\RemoveVehicleFileResponse;
use Application\Vehicle\Shared\VehicleErrorMessageConstants;
use Application\Vehicle\Shared\VehicleFileErrorMessageConstants;
use Domain\Vehicle\Entity\Vehicle;
use Domain\Vehicle\Entity\VehicleFile;
use Domain\Vehicle\Repository\VehicleRepositoryInterface;
use Domain\Vehicle\VehicleFileType;
use PHPUnit\Framework\TestCase;
use Tests\_Mock\Repository\Vehicle\VehicleInMemoryRepository;

class RemoveVehicleFileTest extends TestCase implements RemoveVehicleFilePresenterInterface
{
    private const USER_ID = 'user-id';
    private const VEHICLE_ID = 'vehicle-id';
    private const VEHICLE_FILE_ID = 'vehicle-file-id';
    private const MAKE = 'Nissan';
    private const MODEL = 'Interstar';
    private const FIRST_REGISTRATION_DATE = '2010-05-06';
    private const REGISTRATION_NUMBER = 'AR-659-RV';
    private const PURCHASE_PRICE = 700000;
    private const MILEAGE_AT_PURCHASE = 220000;
    private const CURRENT_KNOWN_MILEAGE = 220000;
    private const LAST_OIL_CHANGE_DATE = '2024-04-04';
    private const LAST_OIL_CHANGE_MILEAGE = 222000;
    private const TIMING_BELT_SERVICE_INTERVALS_IN_YEAR = 5;
    private const TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE = 120000;
    private const LAST_TIMING_BELT_CHANGE_MILEAGE = 210000;
    private const LAST_TIMING_BELT_CHANGE_DATE = '2022-06-06';
    private const LAST_TECHNICAL_INSPECTION_DATE = '2024-01-18';
    private const LAST_POLLUTION_CONTROL_DATE = '2023-06-12';

    private VehicleRepositoryInterface $vehicleRepository;
    private RemoveVehicleFileInterface $removeVehicleFile;
    private RemoveVehicleFileResponse $response;

    public function present(RemoveVehicleFileResponse $response): void
    {
        $this->response = $response;
    }

    public function testVehicleNotFound(): void
    {
        $request = new RemoveVehicleFileRequest('not-existing-id', self::VEHICLE_FILE_ID);
        $this->removeVehicleFile->execute($request, $this);

        $errorNotification = (new ErrorNotification())
            ->add('vehicleId', VehicleErrorMessageConstants::VEHICLE_NOT_FOUND)
        ;

        $this->assertEquals($errorNotification, $this->response->getErrorNotification());
    }

    public function testVehicleFileNotFound(): void
    {
        $this->createVehicle();

        $request = new RemoveVehicleFileRequest(self::VEHICLE_ID, 'not-existing-id');
        $this->removeVehicleFile->execute($request, $this);

        $errorNotification = (new ErrorNotification())
            ->add('vehicleFileId', VehicleFileErrorMessageConstants::VEHICLE_FILE_NOT_FOUND);

        $this->assertEquals($errorNotification, $this->response->getErrorNotification());
    }

    public function testRemoveFile(): void
    {
        $vehicle = $this->createVehicle();
        $vehicleFile = new VehicleFile(
            self::VEHICLE_FILE_ID,
            self::VEHICLE_ID,
            VehicleFileType::INSURANCE
        );

        $vehicle->addInsurance($vehicleFile);
        $this->vehicleRepository->update($vehicle);

        $request = new RemoveVehicleFileRequest(self::VEHICLE_ID, self::VEHICLE_FILE_ID);
        $this->removeVehicleFile->execute($request, $this);

        $this->assertFalse($this->response->getErrorNotification()->hasError());
        $this->assertTrue($this->response->isFileWasDeleted());
    }

    protected function setUp(): void
    {
        $this->vehicleRepository = new VehicleInMemoryRepository();
        $this->removeVehicleFile = new RemoveVehicleFile($this->vehicleRepository);
    }

    private function createVehicle(): Vehicle
    {
        $vehicle = new Vehicle(
            self::VEHICLE_ID,
            self::USER_ID,
            self::MAKE,
            self::MODEL,
            new \DateTimeImmutable(self::FIRST_REGISTRATION_DATE),
            self::REGISTRATION_NUMBER,
            self::PURCHASE_PRICE,
            self::MILEAGE_AT_PURCHASE,
            self::CURRENT_KNOWN_MILEAGE,
            new \DateTimeImmutable(self::LAST_OIL_CHANGE_DATE),
            self::LAST_OIL_CHANGE_MILEAGE,
            self::TIMING_BELT_SERVICE_INTERVALS_IN_YEAR,
            self::TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE,
            new \DateTimeImmutable(self::LAST_TIMING_BELT_CHANGE_DATE),
            self::LAST_TIMING_BELT_CHANGE_MILEAGE,
            new \DateTimeImmutable(self::LAST_TECHNICAL_INSPECTION_DATE),
            new \DateTimeImmutable(self::LAST_POLLUTION_CONTROL_DATE),
        );

        $this->vehicleRepository->create($vehicle);

        return $vehicle;
    }
}
