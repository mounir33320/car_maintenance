<?php

namespace Tests\Unit\Application\Vehicle\UpdateVehicle;

use Application\Vehicle\UpdateVehicle\UpdateVehicleRequest;

class UpdateVehicleRequestBuilder
{
    private string $vehicleId;
    private string $userId;
    private string $make;
    private string $model;
    private string $firstRegistrationDateISO;
    private string $registrationNumber = '';
    private ?int $purchasePrice = null;
    private ?int $mileageAtPurchase = null;
    private ?int $currentKnownMileage = null;
    private ?string $lastOilChangeDateISO = null;
    private ?int $lastOilChangeMileage = null;
    private ?int $timingBeltServiceIntervalsInYears = null;
    private ?int $timingBeltServiceIntervalsInMileage = null;
    private ?string $lastTimingBeltChangeISO = null;
    private ?int $lastTimingBeltChangeMileage = null;
    private ?string $lastTechnicalInspectionISO = null;
    private ?string $lastPollutionControlISO = null;

    public function withVehicleId(string $vehicleId): UpdateVehicleRequestBuilder
    {
        $this->vehicleId = $vehicleId;

        return $this;
    }

    public function withUserId(string $userId): UpdateVehicleRequestBuilder
    {
        $this->userId = $userId;

        return $this;
    }

    public function withMake(string $make): UpdateVehicleRequestBuilder
    {
        $this->make = $make;

        return $this;
    }

    public function withModel(string $model): UpdateVehicleRequestBuilder
    {
        $this->model = $model;

        return $this;
    }

    public function withFirstRegistrationDateISO(string $firstRegistrationDateISO): UpdateVehicleRequestBuilder
    {
        $this->firstRegistrationDateISO = $firstRegistrationDateISO;

        return $this;
    }

    public function withRegistrationNumber(string $registrationNumber): UpdateVehicleRequestBuilder
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    public function withPurchasePrice(?int $purchasePrice): UpdateVehicleRequestBuilder
    {
        $this->purchasePrice = $purchasePrice;

        return $this;
    }

    public function withMileageAtPurchase(?int $mileageAtPurchase): UpdateVehicleRequestBuilder
    {
        $this->mileageAtPurchase = $mileageAtPurchase;

        return $this;
    }

    public function withCurrentKnownMileage(?int $currentKnownMileage): UpdateVehicleRequestBuilder
    {
        $this->currentKnownMileage = $currentKnownMileage;

        return $this;
    }

    public function withLastOilChangeDateISO(?string $lastOilChangeDateISO): UpdateVehicleRequestBuilder
    {
        $this->lastOilChangeDateISO = $lastOilChangeDateISO;

        return $this;
    }

    public function withLastOilChangeMileage(?int $lastOilChangeMileage): UpdateVehicleRequestBuilder
    {
        $this->lastOilChangeMileage = $lastOilChangeMileage;

        return $this;
    }

    public function withTimingBeltServiceIntervalsInYears(?int $timingBeltServiceIntervalsInYears): UpdateVehicleRequestBuilder
    {
        $this->timingBeltServiceIntervalsInYears = $timingBeltServiceIntervalsInYears;

        return $this;
    }

    public function withTimingBeltServiceIntervalsInMileage(?int $timingBeltServiceIntervalsInMileage): UpdateVehicleRequestBuilder
    {
        $this->timingBeltServiceIntervalsInMileage = $timingBeltServiceIntervalsInMileage;

        return $this;
    }

    public function withLastTimingBeltChangeISO(?string $lastTimingBeltChangeISO): UpdateVehicleRequestBuilder
    {
        $this->lastTimingBeltChangeISO = $lastTimingBeltChangeISO;

        return $this;
    }

    public function withLastTimingBeltChangeMileage(?int $lastTimingBeltChangeMileage): UpdateVehicleRequestBuilder
    {
        $this->lastTimingBeltChangeMileage = $lastTimingBeltChangeMileage;

        return $this;
    }

    public function withLastTechnicalInspectionISO(?string $lastTechnicalInspectionISO): UpdateVehicleRequestBuilder
    {
        $this->lastTechnicalInspectionISO = $lastTechnicalInspectionISO;

        return $this;
    }

    public function withLastPollutionControlISO(?string $lastPollutionControlISO): UpdateVehicleRequestBuilder
    {
        $this->lastPollutionControlISO = $lastPollutionControlISO;

        return $this;
    }

    public function build(): UpdateVehicleRequest
    {
        return new UpdateVehicleRequest(
            $this->vehicleId,
            $this->userId,
            $this->make,
            $this->model,
            $this->firstRegistrationDateISO,
            $this->registrationNumber,
            $this->purchasePrice,
            $this->mileageAtPurchase,
            $this->currentKnownMileage,
            $this->lastOilChangeDateISO,
            $this->lastOilChangeMileage,
            $this->timingBeltServiceIntervalsInYears,
            $this->timingBeltServiceIntervalsInMileage,
            $this->lastTimingBeltChangeISO,
            $this->lastTimingBeltChangeMileage,
            $this->lastTechnicalInspectionISO,
            $this->lastPollutionControlISO
        );
    }
}
