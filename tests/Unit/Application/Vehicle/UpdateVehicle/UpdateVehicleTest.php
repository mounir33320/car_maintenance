<?php

namespace Tests\Unit\Application\Vehicle\UpdateVehicle;

use Application\Shared\Error\ErrorNotification;
use Application\User\Shared\UserErrorMessageConstants;
use Application\Vehicle\Shared\VehicleErrorMessageConstants;
use Application\Vehicle\UpdateVehicle\UpdateVehicle;
use Application\Vehicle\UpdateVehicle\UpdateVehicleInterface;
use Application\Vehicle\UpdateVehicle\UpdateVehiclePresenterInterface;
use Application\Vehicle\UpdateVehicle\UpdateVehicleRequest;
use Application\Vehicle\UpdateVehicle\UpdateVehicleResponse;
use Domain\User\Entity\User;
use Domain\User\Repository\UserRepositoryInterface;
use Domain\User\RoleEnum;
use Domain\Vehicle\Entity\Vehicle;
use Domain\Vehicle\Repository\VehicleRepositoryInterface;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Tests\_Mock\Repository\User\UserInMemoryRepository;
use Tests\_Mock\Repository\Vehicle\VehicleInMemoryRepository;

use function PHPUnit\Framework\assertNotNull;

class UpdateVehicleTest extends TestCase implements UpdateVehiclePresenterInterface
{
    private UpdateVehicleResponse $response;
    private VehicleRepositoryInterface $vehicleRepository;
    private UserRepositoryInterface $userRepository;
    private UpdateVehicleInterface $updateVehicle;

    private const VEHICLE_ID = 'vehicle-id';
    private const USER_ID = 'user-id';
    private const MAKE = 'Nissan';
    private const MODEL = 'Interstar';
    private const FIRST_REGISTRATION_DATE = '2010-05-06';
    private const REGISTRATION_NUMBER = 'AR-659-RV';
    private const PURCHASE_PRICE = 700000;
    private const MILEAGE_AT_PURCHASE = 220000;
    private const CURRENT_KNOWN_MILEAGE = 220000;
    private const LAST_OIL_CHANGE_DATE = '2024-04-04';
    private const LAST_OIL_CHANGE_MILEAGE = 222000;
    private const TIMING_BELT_SERVICE_INTERVALS_IN_YEAR = 5;
    private const TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE = 120000;
    private const LAST_TIMING_BELT_CHANGE_MILEAGE = 210000;
    private const LAST_TIMING_BELT_CHANGE_DATE = '2022-06-06';
    private const LAST_TECHNICAL_INSPECTION_DATE = '2024-01-18';
    private const LAST_POLLUTION_CONTROL_DATE = '2023-06-12';

    public function present(UpdateVehicleResponse $response): void
    {
        $this->response = $response;
    }

    public function testVehicleNotFound(): void
    {
        $this->createUser();
        $request = new UpdateVehicleRequest(
            'not-existing-vehicle',
            self::USER_ID,
            self::MAKE,
            self::MODEL,
            self::FIRST_REGISTRATION_DATE
        );

        $this->updateVehicle->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'id',
            VehicleErrorMessageConstants::VEHICLE_NOT_FOUND
        );

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testUserNotFound(): void
    {
        $request = new UpdateVehicleRequest(
            self::VEHICLE_ID,
            'user-id-not-existing',
            self::MAKE,
            self::MODEL,
            self::FIRST_REGISTRATION_DATE
        );

        $this->updateVehicle->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'userId',
            UserErrorMessageConstants::USER_NOT_FOUND
        );

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    /**
     * @throws \Exception
     */
    #[DataProvider('vehicleValuesProvider')]
    public function testValues(
        UpdateVehicleRequest $request,
        bool $hasError,
        ?string $field = null,
        ?string $message = null
    ): void {
        $this->createUser();
        $this->createVehicle();

        $this->updateVehicle->execute($request, $this);

        if ($hasError) {
            assertNotNull($field);
            assertNotNull($message);
            $expectedErrorNotif = (new ErrorNotification())->add($field, $message);

            $this->assertEquals($expectedErrorNotif, $this->response->getErrorNotification());
        } else {
            $vehicles = $this->vehicleRepository->findByUserId(self::USER_ID);
            $this->assertEquals($vehicles[0], $this->response->getUpdatedVehicle());
        }
    }

    /**
     * @return array<string, array{0: UpdateVehicleRequest, 1: bool, 2?: string, 3?: string}>
     */
    public static function vehicleValuesProvider(): array
    {
        $requestBuilder = (new UpdateVehicleRequestBuilder())
            ->withVehicleId(self::VEHICLE_ID)
            ->withUserId(self::USER_ID)
            ->withMake(self::MAKE)
            ->withModel(self::MODEL)
            ->withFirstRegistrationDateISO(self::FIRST_REGISTRATION_DATE)
            ->withRegistrationNumber(self::REGISTRATION_NUMBER)
            ->withPurchasePrice(self::PURCHASE_PRICE)
            ->withMileageAtPurchase(self::MILEAGE_AT_PURCHASE)
            ->withCurrentKnownMileage(self::CURRENT_KNOWN_MILEAGE)
            ->withLastOilChangeDateISO(self::LAST_OIL_CHANGE_DATE)
            ->withLastOilChangeMileage(self::LAST_OIL_CHANGE_MILEAGE)
            ->withTimingBeltServiceIntervalsInYears(self::TIMING_BELT_SERVICE_INTERVALS_IN_YEAR)
            ->withTimingBeltServiceIntervalsInMileage(self::TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE)
            ->withLastTimingBeltChangeISO(self::LAST_TIMING_BELT_CHANGE_DATE)
            ->withLastTimingBeltChangeMileage(self::LAST_TIMING_BELT_CHANGE_MILEAGE)
            ->withLastTechnicalInspectionISO(self::LAST_TECHNICAL_INSPECTION_DATE)
            ->withLastPollutionControlISO(self::LAST_POLLUTION_CONTROL_DATE)
        ;

        return [
            'First Registration Date' => [
                $requestBuilder->withFirstRegistrationDateISO('04-02-1990')->build(),
                true,
                'firstRegistrationDate',
                VehicleErrorMessageConstants::INVALID_DATE_FORMAT
            ],
            'Last Oil Change Date' => [
                $requestBuilder
                    ->withFirstRegistrationDateISO(self::FIRST_REGISTRATION_DATE)
                    ->withLastOilChangeDateISO('04-02-1990')
                    ->build(),
                true,
                'lastOilChangeDate',
                VehicleErrorMessageConstants::INVALID_DATE_FORMAT
            ],
            'Last Timing Belt Change Date' => [
                $requestBuilder
                    ->withLastOilChangeDateISO(self::LAST_OIL_CHANGE_DATE)
                    ->withLastTimingBeltChangeISO('04-02-1990')
                    ->build(),
                true,
                'lastTimingBeltChangeDate',
                VehicleErrorMessageConstants::INVALID_DATE_FORMAT
            ],
            'Last technical inspection Date' => [
                $requestBuilder
                    ->withLastTimingBeltChangeISO(self::LAST_TIMING_BELT_CHANGE_DATE)
                    ->withLastTechnicalInspectionISO('04-02-1990')
                    ->build(),
                true,
                'lastTechnicalInspection',
                VehicleErrorMessageConstants::INVALID_DATE_FORMAT
            ],
            'Last pollution control Date' => [
                $requestBuilder
                    ->withLastTechnicalInspectionISO(self::LAST_TECHNICAL_INSPECTION_DATE)
                    ->withLastPollutionControlISO('04-02-1990')
                    ->build(),
                true,
                'lastPollutionControl',
                VehicleErrorMessageConstants::INVALID_DATE_FORMAT
            ],
            'Invalid registration number' => [
                $requestBuilder
                    ->withLastPollutionControlISO(self::LAST_POLLUTION_CONTROL_DATE)
                    ->withRegistrationNumber('INVALID')
                    ->build(),
                true,
                'registrationNumber',
                VehicleErrorMessageConstants::INVALID_REGISTRATION_NUMBER
            ],
            'Negative purchase price' => [
                $requestBuilder
                    ->withRegistrationNumber(self::REGISTRATION_NUMBER)
                    ->withPurchasePrice(-500000)
                    ->build(),
                true,
                'purchasePrice',
                VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE
            ],
            'Too large purchase price' => [
                $requestBuilder
                    ->withPurchasePrice(100000001)
                    ->build(),
                true,
                'purchasePrice',
                VehicleErrorMessageConstants::TOO_LARGE_NUMBER
            ],
            'Negative current known mileage' => [
                $requestBuilder
                    ->withPurchasePrice(self::PURCHASE_PRICE)
                    ->withCurrentKnownMileage(-5000)
                    ->build(),
                true,
                'currentKnownMileage',
                VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE
            ],
            'Too large current mileage' => [
                $requestBuilder
                    ->withCurrentKnownMileage(100000001)
                    ->build(),
                true,
                'currentKnownMileage',
                VehicleErrorMessageConstants::TOO_LARGE_NUMBER
            ],
            'Negative last oil change mileage' => [
                $requestBuilder
                    ->withCurrentKnownMileage(self::CURRENT_KNOWN_MILEAGE)
                    ->withLastOilChangeMileage(-50)
                    ->build(),
                true,
                'lastOilChangeMileage',
                VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE
            ],
            'Too large last oil change mileage' => [
                $requestBuilder
                    ->withLastOilChangeMileage(100000001)
                    ->build(),
                true,
                'lastOilChangeMileage',
                VehicleErrorMessageConstants::TOO_LARGE_NUMBER
            ],
            'Negative timing belt service interval in years' => [
                $requestBuilder
                    ->withLastOilChangeMileage(self::LAST_OIL_CHANGE_MILEAGE)
                    ->withTimingBeltServiceIntervalsInYears(-5)
                    ->build(),
                true,
                'timingBeltServiceIntervalsInYears',
                VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE
            ],
            'Too large timing belt service interval in years' => [
                $requestBuilder
                    ->withTimingBeltServiceIntervalsInYears(31)
                    ->build(),
                true,
                'timingBeltServiceIntervalsInYears',
                VehicleErrorMessageConstants::TOO_LARGE_NUMBER
            ],
            'Negative timing belt service intervals in mileage' => [
                $requestBuilder
                    ->withTimingBeltServiceIntervalsInYears(self::TIMING_BELT_SERVICE_INTERVALS_IN_YEAR)
                    ->withTimingBeltServiceIntervalsInMileage(-5)
                    ->build(),
                true,
                'timingBeltServiceIntervalsInMileage',
                VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE
            ],
            'Too large timing belt service intervals in mileage' => [
                $requestBuilder
                    ->withTimingBeltServiceIntervalsInMileage(500001)
                    ->build(),
                true,
                'timingBeltServiceIntervalsInMileage',
                VehicleErrorMessageConstants::TOO_LARGE_NUMBER
            ],
            'Negative last timing belt change mileage' => [
                $requestBuilder
                    ->withTimingBeltServiceIntervalsInMileage(self::TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE)
                    ->withLastTimingBeltChangeMileage(-5)
                    ->build(),
                true,
                'lastTimingBeltChangeMileage',
                VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE
            ],
            'Too large last timing belt change mileage' => [
                $requestBuilder
                    ->withLastTimingBeltChangeMileage(500001)
                    ->build(),
                true,
                'lastTimingBeltChangeMileage',
                VehicleErrorMessageConstants::TOO_LARGE_NUMBER
            ],
            'Valid Request' => [
                $requestBuilder
                    ->withLastTimingBeltChangeMileage(self::LAST_TIMING_BELT_CHANGE_MILEAGE)
                    ->build(),
                false
            ],
        ];
    }

    public function testUpdateVehicle(): void
    {
        $this->createUser();
        $this->createVehicle();

        $make = 'Ford';
        $model = 'Ka';
        $firstRegistrationDate = '2019-01-01';
        $registrationNumber = 'AA-123-BB';
        $purchasePrice = 650000;

        $request = new UpdateVehicleRequest(
            self::VEHICLE_ID,
            self::USER_ID,
            $make,
            $model,
            $firstRegistrationDate,
            $registrationNumber,
            $purchasePrice,
            self::MILEAGE_AT_PURCHASE,
            self::CURRENT_KNOWN_MILEAGE,
            self::LAST_OIL_CHANGE_DATE,
            self::LAST_OIL_CHANGE_MILEAGE,
            self::TIMING_BELT_SERVICE_INTERVALS_IN_YEAR,
            self::TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE,
            self::LAST_TIMING_BELT_CHANGE_DATE,
            self::LAST_TIMING_BELT_CHANGE_MILEAGE,
            self::LAST_TECHNICAL_INSPECTION_DATE,
            self::LAST_POLLUTION_CONTROL_DATE
        );

        $this->updateVehicle->execute($request, $this);

        $this->assertEquals(self::USER_ID, $this->response->getUpdatedVehicle()?->userId());
        $this->assertEquals($make, $this->response->getUpdatedVehicle()?->make());
        $this->assertEquals($model, $this->response->getUpdatedVehicle()?->model());
        $this->assertEquals(
            new \DateTimeImmutable($firstRegistrationDate),
            $this->response->getUpdatedVehicle()?->firstRegistrationDate()
        );
        $this->assertEquals($registrationNumber, $this->response->getUpdatedVehicle()?->registrationNumber());
        $this->assertEquals($purchasePrice, $this->response->getUpdatedVehicle()?->purchasePrice());
        $this->assertEquals(
            self::MILEAGE_AT_PURCHASE,
            $this->response->getUpdatedVehicle()?->mileageAtPurchase()
        );
        $this->assertEquals(
            self::CURRENT_KNOWN_MILEAGE,
            $this->response->getUpdatedVehicle()?->currentKnownMileage()
        );
        $this->assertEquals(
            new \DateTimeImmutable(self::LAST_OIL_CHANGE_DATE),
            $this->response->getUpdatedVehicle()?->lastOilChangeDate()
        );
        $this->assertEquals(
            self::LAST_OIL_CHANGE_MILEAGE,
            $this->response->getUpdatedVehicle()?->lastOilChangeMileage()
        );
        $this->assertEquals(
            self::TIMING_BELT_SERVICE_INTERVALS_IN_YEAR,
            $this->response->getUpdatedVehicle()?->timingBeltServiceIntervalsInYears()
        );
        $this->assertEquals(
            self::TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE,
            $this->response->getUpdatedVehicle()?->timingBeltServiceIntervalsInMileage()
        );
        $this->assertEquals(
            new \DateTimeImmutable(self::LAST_TIMING_BELT_CHANGE_DATE),
            $this->response->getUpdatedVehicle()?->lastTimingBeltChangeDate()
        );
        $this->assertEquals(
            self::LAST_TIMING_BELT_CHANGE_MILEAGE,
            $this->response->getUpdatedVehicle()?->lastTimingBeltChangeMileage()
        );
        $this->assertEquals(
            new \DateTimeImmutable(self::LAST_TECHNICAL_INSPECTION_DATE),
            $this->response->getUpdatedVehicle()?->lastTechnicalInspectionDate()
        );
        $this->assertEquals(
            new \DateTimeImmutable(self::LAST_POLLUTION_CONTROL_DATE),
            $this->response->getUpdatedVehicle()?->lastPollutionControlDate()
        );
    }

    protected function setUp(): void
    {
        $this->vehicleRepository = new VehicleInMemoryRepository();
        $this->userRepository = new UserInMemoryRepository();

        $this->updateVehicle = new UpdateVehicle($this->vehicleRepository, $this->userRepository);
    }

    private function createUser(): void
    {
        $user = new User(self::USER_ID, 'test@test.fr', 'password', RoleEnum::RoleUser);

        $this->userRepository->createUser($user);
    }

    private function createVehicle(): void
    {
        $vehicle = new Vehicle(
            self::VEHICLE_ID,
            self::USER_ID,
            self::MAKE,
            self::MODEL,
            new \DateTimeImmutable(self::FIRST_REGISTRATION_DATE),
            self::REGISTRATION_NUMBER,
            self::PURCHASE_PRICE,
            self::MILEAGE_AT_PURCHASE,
            self::CURRENT_KNOWN_MILEAGE,
            new \DateTimeImmutable(self::LAST_OIL_CHANGE_DATE),
            self::LAST_OIL_CHANGE_MILEAGE,
            self::TIMING_BELT_SERVICE_INTERVALS_IN_YEAR,
            self::TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE,
            new \DateTimeImmutable(self::LAST_TIMING_BELT_CHANGE_DATE),
            self::LAST_TIMING_BELT_CHANGE_MILEAGE,
            new \DateTimeImmutable(self::LAST_TECHNICAL_INSPECTION_DATE),
            new \DateTimeImmutable(self::LAST_POLLUTION_CONTROL_DATE)
        );

        $this->vehicleRepository->create($vehicle);
    }
}
