<?php

namespace Tests\Unit\Application\Vehicle\DeleteVehicle;

use Application\Shared\Error\ErrorNotification;
use Application\User\Shared\UserErrorMessageConstants;
use Application\Vehicle\DeleteVehicle\DeleteVehicle;
use Application\Vehicle\DeleteVehicle\DeleteVehicleInterface;
use Application\Vehicle\DeleteVehicle\DeleteVehiclePresenterInterface;
use Application\Vehicle\DeleteVehicle\DeleteVehicleRequest;
use Application\Vehicle\DeleteVehicle\DeleteVehicleResponse;
use Application\Vehicle\Shared\VehicleErrorMessageConstants;
use Domain\User\Entity\User;
use Domain\User\Repository\UserRepositoryInterface;
use Domain\User\RoleEnum;
use Domain\Vehicle\Entity\Vehicle;
use Domain\Vehicle\Repository\VehicleRepositoryInterface;
use PHPUnit\Framework\TestCase;
use Tests\_Mock\Repository\User\UserInMemoryRepository;
use Tests\_Mock\Repository\Vehicle\VehicleInMemoryRepository;

class DeleteVehicleTest extends TestCase implements DeleteVehiclePresenterInterface
{
    private const VEHICLE_ID = 'vehicle-id';
    private const USER_ID = 'user-id';

    private const MAKE = 'Nissan';
    private const MODEL = 'Interstar';
    private const FIRST_REGISTRATION_DATE = '2010-05-06';
    private const REGISTRATION_NUMBER = 'AR-659-RV';
    private const PURCHASE_PRICE = 700000;
    private const MILEAGE_AT_PURCHASE = 220000;
    private const CURRENT_KNOWN_MILEAGE = 220000;
    private const LAST_OIL_CHANGE_DATE = '2024-04-04';
    private const LAST_OIL_CHANGE_MILEAGE = 222000;
    private const TIMING_BELT_SERVICE_INTERVALS_IN_YEAR = 5;
    private const TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE = 120000;
    private const LAST_TIMING_BELT_CHANGE_MILEAGE = 210000;
    private const LAST_TIMING_BELT_CHANGE_DATE = '2022-06-06';
    private const LAST_TECHNICAL_INSPECTION = '2024-01-18';
    private const LAST_POLLUTION_CONTROL = '2023-06-12';

    private DeleteVehicleResponse $response;
    private VehicleRepositoryInterface $vehicleRepository;
    private UserRepositoryInterface $userRepository;

    private DeleteVehicleInterface $deleteVehicle;

    public function present(DeleteVehicleResponse $response): void
    {
        $this->response = $response;
    }

    public function testUserNotFound(): void
    {
        $request = new DeleteVehicleRequest(self::VEHICLE_ID, self::USER_ID);

        $this->deleteVehicle->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('userId', UserErrorMessageConstants::USER_NOT_FOUND);

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testVehicleNotFound(): void
    {
        $this->createUser();
        $request = new DeleteVehicleRequest(
            self::VEHICLE_ID,
            self::USER_ID
        );

        $this->deleteVehicle->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add(
            'id',
            VehicleErrorMessageConstants::VEHICLE_NOT_FOUND
        );

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    public function testDeleteVehicle(): void
    {
        $this->createUser();
        $this->createVehicle();

        $this->assertCount(1, $this->vehicleRepository->findByUserId(self::USER_ID));

        $request = new DeleteVehicleRequest(self::VEHICLE_ID, self::USER_ID);
        $this->deleteVehicle->execute($request, $this);

        $this->assertCount(0, $this->vehicleRepository->findByUserId(self::USER_ID));
    }

    protected function setUp(): void
    {
        $this->vehicleRepository = new VehicleInMemoryRepository();
        $this->userRepository = new UserInMemoryRepository();
        $this->deleteVehicle = new DeleteVehicle($this->vehicleRepository, $this->userRepository);
    }

    private function createUser(): void
    {
        $user = new User(self::USER_ID, 'test@test.fr', 'password', RoleEnum::RoleUser);

        $this->userRepository->createUser($user);
    }

    private function createVehicle(): void
    {
        $vehicle = new Vehicle(
            self::VEHICLE_ID,
            self::USER_ID,
            self::MAKE,
            self::MODEL,
            new \DateTimeImmutable(self::FIRST_REGISTRATION_DATE),
            self::REGISTRATION_NUMBER,
            self::PURCHASE_PRICE,
            self::MILEAGE_AT_PURCHASE,
            self::CURRENT_KNOWN_MILEAGE,
            new \DateTimeImmutable(self::LAST_OIL_CHANGE_DATE),
            self::LAST_OIL_CHANGE_MILEAGE,
            self::TIMING_BELT_SERVICE_INTERVALS_IN_YEAR,
            self::TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE,
            new \DateTimeImmutable(self::LAST_TIMING_BELT_CHANGE_DATE),
            self::LAST_TIMING_BELT_CHANGE_MILEAGE,
            new \DateTimeImmutable(self::LAST_TECHNICAL_INSPECTION),
            new \DateTimeImmutable(self::LAST_POLLUTION_CONTROL)
        );

        $this->vehicleRepository->create($vehicle);
    }
}
