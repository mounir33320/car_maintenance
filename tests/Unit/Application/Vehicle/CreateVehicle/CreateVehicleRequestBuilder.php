<?php

namespace Tests\Unit\Application\Vehicle\CreateVehicle;

use Application\Vehicle\CreateVehicle\CreateVehicleRequest;

class CreateVehicleRequestBuilder
{
    private string $userId;
    private string $make;
    private string $model;
    private string $firstRegistrationDateISO;
    private string $registrationNumber = '';
    private ?int $purchasePrice = null;
    private ?int $mileageAtPurchase = null;
    private ?int $currentKnownMileage = null;
    private ?string $lastOilChangeDateISO = null;
    private ?int $lastOilChangeMileage = null;
    private ?int $timingBeltServiceIntervalsInYears = null;
    private ?int $timingBeltServiceIntervalsInMileage = null;
    private ?string $lastTimingBeltChangeISO = null;
    private ?int $lastTimingBeltChangeMileage = null;
    private ?string $lastTechnicalInspectionISO = null;
    private ?string $lastPollutionControlISO = null;

    public function withUserId(string $userId): CreateVehicleRequestBuilder
    {
        $this->userId = $userId;

        return $this;
    }

    public function withMake(string $make): CreateVehicleRequestBuilder
    {
        $this->make = $make;

        return $this;
    }

    public function withModel(string $model): CreateVehicleRequestBuilder
    {
        $this->model = $model;

        return $this;
    }

    public function withFirstRegistrationDateISO(string $firstRegistrationDateISO): CreateVehicleRequestBuilder
    {
        $this->firstRegistrationDateISO = $firstRegistrationDateISO;

        return $this;
    }

    public function withRegistrationNumber(string $registrationNumber): CreateVehicleRequestBuilder
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    public function withPurchasePrice(?int $purchasePrice): CreateVehicleRequestBuilder
    {
        $this->purchasePrice = $purchasePrice;

        return $this;
    }

    public function withMileageAtPurchase(?int $mileageAtPurchase): CreateVehicleRequestBuilder
    {
        $this->mileageAtPurchase = $mileageAtPurchase;

        return $this;
    }

    public function withCurrentKnownMileage(?int $currentKnownMileage): CreateVehicleRequestBuilder
    {
        $this->currentKnownMileage = $currentKnownMileage;

        return $this;
    }

    public function withLastOilChangeDateISO(?string $lastOilChangeDateISO): CreateVehicleRequestBuilder
    {
        $this->lastOilChangeDateISO = $lastOilChangeDateISO;

        return $this;
    }

    public function withLastOilChangeMileage(?int $lastOilChangeMileage): CreateVehicleRequestBuilder
    {
        $this->lastOilChangeMileage = $lastOilChangeMileage;

        return $this;
    }

    public function withTimingBeltServiceIntervalsInYears(?int $timingBeltServiceIntervalsInYears): CreateVehicleRequestBuilder
    {
        $this->timingBeltServiceIntervalsInYears = $timingBeltServiceIntervalsInYears;

        return $this;
    }

    public function withTimingBeltServiceIntervalsInMileage(?int $timingBeltServiceIntervalsInMileage): CreateVehicleRequestBuilder
    {
        $this->timingBeltServiceIntervalsInMileage = $timingBeltServiceIntervalsInMileage;

        return $this;
    }

    public function withLastTimingBeltChangeISO(?string $lastTimingBeltChangeISO): CreateVehicleRequestBuilder
    {
        $this->lastTimingBeltChangeISO = $lastTimingBeltChangeISO;

        return $this;
    }

    public function withLastTimingBeltChangeMileage(?int $lastTimingBeltChangeMileage): CreateVehicleRequestBuilder
    {
        $this->lastTimingBeltChangeMileage = $lastTimingBeltChangeMileage;

        return $this;
    }

    public function withLastTechnicalInspectionISO(?string $lastTechnicalInspectionISO): CreateVehicleRequestBuilder
    {
        $this->lastTechnicalInspectionISO = $lastTechnicalInspectionISO;

        return $this;
    }

    public function withLastPollutionControlISO(?string $lastPollutionControlISO): CreateVehicleRequestBuilder
    {
        $this->lastPollutionControlISO = $lastPollutionControlISO;

        return $this;
    }

    public function build(): CreateVehicleRequest
    {
        return new CreateVehicleRequest(
            $this->userId,
            $this->make,
            $this->model,
            $this->firstRegistrationDateISO,
            $this->registrationNumber,
            $this->purchasePrice,
            $this->mileageAtPurchase,
            $this->currentKnownMileage,
            $this->lastOilChangeDateISO,
            $this->lastOilChangeMileage,
            $this->timingBeltServiceIntervalsInYears,
            $this->timingBeltServiceIntervalsInMileage,
            $this->lastTimingBeltChangeISO,
            $this->lastTimingBeltChangeMileage,
            $this->lastTechnicalInspectionISO,
            $this->lastPollutionControlISO
        );
    }
}
