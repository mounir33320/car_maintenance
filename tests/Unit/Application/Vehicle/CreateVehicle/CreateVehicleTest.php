<?php

namespace Tests\Unit\Application\Vehicle\CreateVehicle;

use Application\Shared\Error\ErrorNotification;
use Application\User\Shared\UserErrorMessageConstants;
use Application\Vehicle\CreateVehicle\CreateVehicle;
use Application\Vehicle\CreateVehicle\CreateVehicleInterface;
use Application\Vehicle\CreateVehicle\CreateVehiclePresenterInterface;
use Application\Vehicle\CreateVehicle\CreateVehicleRequest;
use Application\Vehicle\CreateVehicle\CreateVehicleResponse;
use Application\Vehicle\Shared\VehicleErrorMessageConstants;
use Domain\User\Entity\User;
use Domain\User\Repository\UserRepositoryInterface;
use Domain\User\RoleEnum;
use Domain\Vehicle\Repository\VehicleRepositoryInterface;
use Infrastructure\Service\UUIDGenerator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Tests\_Mock\Repository\User\UserInMemoryRepository;
use Tests\_Mock\Repository\Vehicle\VehicleInMemoryRepository;

use function PHPUnit\Framework\assertNotNull;

class CreateVehicleTest extends TestCase implements CreateVehiclePresenterInterface
{
    private CreateVehicleResponse $response;
    private VehicleRepositoryInterface $vehicleRepository;
    private UserRepositoryInterface $userRepository;
    private CreateVehicleInterface $createVehicle;

    private const USER_ID = 'user-id';
    private const MAKE = 'Nissan';
    private const MODEL = 'Interstar';
    private const FIRST_REGISTRATION_DATE = '2010-05-06';
    private const REGISTRATION_NUMBER = 'AR-659-RV';
    private const PURCHASE_PRICE = 700000;
    private const MILEAGE_AT_PURCHASE = 220000;
    private const CURRENT_KNOWN_MILEAGE = 220000;
    private const LAST_OIL_CHANGE_DATE = '2024-04-04';
    private const LAST_OIL_CHANGE_MILEAGE = 222000;
    private const TIMING_BELT_SERVICE_INTERVALS_IN_YEAR = 5;
    private const TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE = 120000;
    private const LAST_TIMING_BELT_CHANGE_MILEAGE = 210000;
    private const LAST_TIMING_BELT_CHANGE_DATE = '2022-06-06';
    private const LAST_TECHNICAL_INSPECTION_DATE = '2024-01-18';
    private const LAST_POLLUTION_CONTROL_DATE = '2023-06-12';

    public function present(CreateVehicleResponse $response): void
    {
        $this->response = $response;
    }

    public function testUserNotFound(): void
    {
        $request = new CreateVehicleRequest('user-id-not-existing', self::MAKE, self::MODEL, self::FIRST_REGISTRATION_DATE);

        $this->createVehicle->execute($request, $this);

        $expectedNotif = (new ErrorNotification())->add('userId', UserErrorMessageConstants::USER_NOT_FOUND);

        $this->assertEquals($expectedNotif, $this->response->getErrorNotification());
    }

    #[DataProvider('vehicleValuesProvider')]
    public function testValues(
        CreateVehicleRequest $request,
        bool $hasError,
        ?string $field = null,
        ?string $message = null
    ): void {
        $this->createUser();

        $this->createVehicle->execute($request, $this);

        if ($hasError) {
            assertNotNull($field);
            assertNotNull($message);
            $expectedErrorNotif = (new ErrorNotification())->add($field, $message);

            $this->assertEquals($expectedErrorNotif, $this->response->getErrorNotification());
        } else {
            $vehicles = $this->vehicleRepository->findByUserId(self::USER_ID);
            $this->assertEquals($vehicles[0], $this->response->getVehicle());
        }
    }

    /**
     * @return array<string, array{0: CreateVehicleRequest, 1: bool, 2?: string, 3?: string}>
     */
    public static function vehicleValuesProvider(): array
    {
        $requestBuilder = (new CreateVehicleRequestBuilder())
            ->withUserId(self::USER_ID)
            ->withMake(self::MAKE)
            ->withModel(self::MODEL)
            ->withFirstRegistrationDateISO(self::FIRST_REGISTRATION_DATE)
            ->withRegistrationNumber(self::REGISTRATION_NUMBER)
            ->withPurchasePrice(self::PURCHASE_PRICE)
            ->withMileageAtPurchase(self::MILEAGE_AT_PURCHASE)
            ->withCurrentKnownMileage(self::CURRENT_KNOWN_MILEAGE)
            ->withLastOilChangeDateISO(self::LAST_OIL_CHANGE_DATE)
            ->withLastOilChangeMileage(self::LAST_OIL_CHANGE_MILEAGE)
            ->withTimingBeltServiceIntervalsInYears(self::TIMING_BELT_SERVICE_INTERVALS_IN_YEAR)
            ->withTimingBeltServiceIntervalsInMileage(self::TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE)
            ->withLastTimingBeltChangeISO(self::LAST_TIMING_BELT_CHANGE_DATE)
            ->withLastTimingBeltChangeMileage(self::LAST_TIMING_BELT_CHANGE_MILEAGE)
            ->withLastTechnicalInspectionISO(self::LAST_TECHNICAL_INSPECTION_DATE)
            ->withLastPollutionControlISO(self::LAST_POLLUTION_CONTROL_DATE)
        ;

        return [
            'First Registration Date' => [
                $requestBuilder->withFirstRegistrationDateISO('04-02-1990')->build(),
                true,
                'firstRegistrationDate',
                VehicleErrorMessageConstants::INVALID_DATE_FORMAT
            ],
            'Last Oil Change Date' => [
                $requestBuilder
                    ->withFirstRegistrationDateISO(self::FIRST_REGISTRATION_DATE)
                    ->withLastOilChangeDateISO('04-02-1990')
                    ->build(),
                true,
                'lastOilChangeDate',
                VehicleErrorMessageConstants::INVALID_DATE_FORMAT
            ],
            'Last Timing Belt Change Date' => [
                $requestBuilder
                    ->withLastOilChangeDateISO(self::LAST_OIL_CHANGE_DATE)
                    ->withLastTimingBeltChangeISO('04-02-1990')
                    ->build(),
                true,
                'lastTimingBeltChangeDate',
                VehicleErrorMessageConstants::INVALID_DATE_FORMAT
            ],
            'Last technical inspection Date' => [
                $requestBuilder
                    ->withLastTimingBeltChangeISO(self::LAST_TIMING_BELT_CHANGE_DATE)
                    ->withLastTechnicalInspectionISO('04-02-1990')
                    ->build(),
                true,
                'lastTechnicalInspection',
                VehicleErrorMessageConstants::INVALID_DATE_FORMAT
            ],
            'Last pollution control Date' => [
                $requestBuilder
                    ->withLastTechnicalInspectionISO(self::LAST_TECHNICAL_INSPECTION_DATE)
                    ->withLastPollutionControlISO('04-02-1990')
                    ->build(),
                true,
                'lastPollutionControl',
                VehicleErrorMessageConstants::INVALID_DATE_FORMAT
            ],
            'Invalid registration number' => [
                $requestBuilder
                    ->withLastPollutionControlISO(self::LAST_POLLUTION_CONTROL_DATE)
                    ->withRegistrationNumber('INVALID')
                    ->build(),
                true,
                'registrationNumber',
                VehicleErrorMessageConstants::INVALID_REGISTRATION_NUMBER
            ],
            'Negative purchase price' => [
                $requestBuilder
                    ->withRegistrationNumber(self::REGISTRATION_NUMBER)
                    ->withPurchasePrice(-500000)
                    ->build(),
                true,
                'purchasePrice',
                VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE
            ],
            'Too large purchase price' => [
                $requestBuilder
                    ->withPurchasePrice(100000001)
                    ->build(),
                true,
                'purchasePrice',
                VehicleErrorMessageConstants::TOO_LARGE_NUMBER
            ],
            'Negative current known mileage' => [
                $requestBuilder
                    ->withPurchasePrice(self::PURCHASE_PRICE)
                    ->withCurrentKnownMileage(-5000)
                    ->build(),
                true,
                'currentKnownMileage',
                VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE
            ],
            'Too large current mileage' => [
                $requestBuilder
                    ->withCurrentKnownMileage(100000001)
                    ->build(),
                true,
                'currentKnownMileage',
                VehicleErrorMessageConstants::TOO_LARGE_NUMBER
            ],
            'Negative last oil change mileage' => [
                $requestBuilder
                    ->withCurrentKnownMileage(self::CURRENT_KNOWN_MILEAGE)
                    ->withLastOilChangeMileage(-50)
                    ->build(),
                true,
                'lastOilChangeMileage',
                VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE
            ],
            'Too large last oil change mileage' => [
                $requestBuilder
                    ->withLastOilChangeMileage(100000001)
                    ->build(),
                true,
                'lastOilChangeMileage',
                VehicleErrorMessageConstants::TOO_LARGE_NUMBER
            ],
            'Negative timing belt service interval in years' => [
                $requestBuilder
                    ->withLastOilChangeMileage(self::LAST_OIL_CHANGE_MILEAGE)
                    ->withTimingBeltServiceIntervalsInYears(-5)
                    ->build(),
                true,
                'timingBeltServiceIntervalsInYears',
                VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE
            ],
            'Too large timing belt service interval in years' => [
                $requestBuilder
                    ->withTimingBeltServiceIntervalsInYears(31)
                    ->build(),
                true,
                'timingBeltServiceIntervalsInYears',
                VehicleErrorMessageConstants::TOO_LARGE_NUMBER
            ],
            'Negative timing belt service intervals in mileage' => [
                $requestBuilder
                    ->withTimingBeltServiceIntervalsInYears(self::TIMING_BELT_SERVICE_INTERVALS_IN_YEAR)
                    ->withTimingBeltServiceIntervalsInMileage(-5)
                    ->build(),
                true,
                'timingBeltServiceIntervalsInMileage',
                VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE
            ],
            'Too large timing belt service intervals in mileage' => [
                $requestBuilder
                    ->withTimingBeltServiceIntervalsInMileage(500001)
                    ->build(),
                true,
                'timingBeltServiceIntervalsInMileage',
                VehicleErrorMessageConstants::TOO_LARGE_NUMBER
            ],
            'Negative last timing belt change mileage' => [
                $requestBuilder
                    ->withTimingBeltServiceIntervalsInMileage(self::TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE)
                    ->withLastTimingBeltChangeMileage(-5)
                    ->build(),
                true,
                'lastTimingBeltChangeMileage',
                VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE
            ],
            'Too large last timing belt change mileage' => [
                $requestBuilder
                    ->withLastTimingBeltChangeMileage(500001)
                    ->build(),
                true,
                'lastTimingBeltChangeMileage',
                VehicleErrorMessageConstants::TOO_LARGE_NUMBER
            ],
            'Valid Request' => [
                $requestBuilder
                    ->withLastTimingBeltChangeMileage(self::LAST_TIMING_BELT_CHANGE_MILEAGE)
                    ->build(),
                false
            ],
        ];
    }

    public function testCreateVehicle(): void
    {
        $this->createUser();
        $request = new CreateVehicleRequest(
            self::USER_ID,
            self::MAKE,
            self::MODEL,
            self::FIRST_REGISTRATION_DATE,
            self::REGISTRATION_NUMBER,
            self::PURCHASE_PRICE,
            self::MILEAGE_AT_PURCHASE,
            self::CURRENT_KNOWN_MILEAGE,
            self::LAST_OIL_CHANGE_DATE,
            self::LAST_OIL_CHANGE_MILEAGE,
            self::TIMING_BELT_SERVICE_INTERVALS_IN_YEAR,
            self::TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE,
            self::LAST_TIMING_BELT_CHANGE_DATE,
            self::LAST_TIMING_BELT_CHANGE_MILEAGE,
            self::LAST_TECHNICAL_INSPECTION_DATE,
            self::LAST_POLLUTION_CONTROL_DATE
        );

        $this->createVehicle->execute($request, $this);

        $this->assertCount(1, $this->vehicleRepository->findByUserId(self::USER_ID));
        $this->assertEquals(self::USER_ID, $this->response->getVehicle()?->userId());
        $this->assertEquals(self::MAKE, $this->response->getVehicle()?->make());
        $this->assertEquals(self::MODEL, $this->response->getVehicle()?->model());
        $this->assertEquals(
            new \DateTimeImmutable(self::FIRST_REGISTRATION_DATE),
            $this->response->getVehicle()?->firstRegistrationDate()
        );
        $this->assertEquals(self::REGISTRATION_NUMBER, $this->response->getVehicle()?->registrationNumber());
        $this->assertEquals(self::PURCHASE_PRICE, $this->response->getVehicle()?->purchasePrice());
        $this->assertEquals(self::MILEAGE_AT_PURCHASE, $this->response->getVehicle()?->mileageAtPurchase());
        $this->assertEquals(self::CURRENT_KNOWN_MILEAGE, $this->response->getVehicle()?->currentKnownMileage());
        $this->assertEquals(
            new \DateTimeImmutable(self::LAST_OIL_CHANGE_DATE),
            $this->response->getVehicle()?->lastOilChangeDate()
        );
        $this->assertEquals(
            self::LAST_OIL_CHANGE_MILEAGE,
            $this->response->getVehicle()?->lastOilChangeMileage()
        );
        $this->assertEquals(
            self::TIMING_BELT_SERVICE_INTERVALS_IN_YEAR,
            $this->response->getVehicle()?->timingBeltServiceIntervalsInYears()
        );
        $this->assertEquals(
            self::TIMING_BELT_SERVICE_INTERVALS_IN_MILEAGE,
            $this->response->getVehicle()?->timingBeltServiceIntervalsInMileage()
        );
        $this->assertEquals(
            new \DateTimeImmutable(self::LAST_TIMING_BELT_CHANGE_DATE),
            $this->response->getVehicle()?->lastTimingBeltChangeDate()
        );
        $this->assertEquals(
            self::LAST_TIMING_BELT_CHANGE_MILEAGE,
            $this->response->getVehicle()?->lastTimingBeltChangeMileage()
        );
        $this->assertEquals(
            new \DateTimeImmutable(self::LAST_TECHNICAL_INSPECTION_DATE),
            $this->response->getVehicle()?->lastTechnicalInspectionDate()
        );
        $this->assertEquals(
            new \DateTimeImmutable(self::LAST_POLLUTION_CONTROL_DATE),
            $this->response->getVehicle()?->lastPollutionControlDate()
        );
    }

    protected function setUp(): void
    {
        $this->vehicleRepository = new VehicleInMemoryRepository();
        $this->userRepository = new UserInMemoryRepository();

        $this->createVehicle = new CreateVehicle($this->vehicleRepository, $this->userRepository, new UUIDGenerator());
    }

    private function createUser(): void
    {
        $user = new User(self::USER_ID, 'test@test.fr', 'password', RoleEnum::RoleUser);

        $this->userRepository->createUser($user);
    }
}
