<?php

namespace Tests\_Mock\Repository\Vehicle;

use Domain\Vehicle\Entity\Vehicle;
use Domain\Vehicle\Repository\VehicleRepositoryInterface;

class VehicleInMemoryRepository implements VehicleRepositoryInterface
{
    /**
     * @var Vehicle[]
     */
    private array $vehicles = [];

    public function findByUserId(string $userId): array
    {
        return array_values(
            array_filter($this->vehicles, function (Vehicle $vehicle) use ($userId) {
                return $vehicle->userId() === $userId;
            })
        );
    }

    public function findById(string $id): ?Vehicle
    {
        $filtered = array_filter($this->vehicles, function (Vehicle $vehicle) use ($id) {
            return $vehicle->id() === $id;
        });

        return !empty($filtered) ? array_pop($filtered) : null;
    }

    public function create(Vehicle $vehicle): void
    {
        $this->vehicles[] = $vehicle;
    }

    public function update(Vehicle $vehicle): void
    {
        for ($i = 0; $i < count($this->vehicles); ++$i) {
            if ($this->vehicles[$i]->id() === $vehicle->id()) {
                $this->vehicles[$i] = $vehicle;
                break;
            }
        }
    }

    public function delete(Vehicle $vehicle): void
    {
        $this->vehicles = array_values(
            array_filter(
                $this->vehicles,
                function (Vehicle $currentVehicle) use ($vehicle) {
                    return $vehicle->id() !== $currentVehicle->id();
                }
            )
        );
    }
}
