<?php

namespace Tests\_Mock\Repository\Vehicle;

use Domain\Vehicle\Entity\Vehicle;
use PHPUnit\Framework\TestCase;

class VehicleInMemoryRepositoryTest extends TestCase
{
    private const VEHICLE_ID = 'vehicleId';
    private const USER_ID = 'userId';
    private const MAKE = 'Nissan';
    private const MODEL = 'Interstar';

    private const REGISTRATION_DATE = '2010-06-05';

    private VehicleInMemoryRepository $repository;

    public function testCreateVehicle(): void
    {
        $this->createTestVehicle();
        $this->assertSame(1, count($this->repository->findByUserId(self::USER_ID)));
    }

    public function testUpdateVehicle(): void
    {
        $this->createTestVehicle();
        $updateVehicule = new Vehicle(
            self::VEHICLE_ID,
            self::USER_ID,
            self::MAKE,
            'Movistar',
            new \DateTimeImmutable('2014-06-25')
        );

        $this->repository->update($updateVehicule);
        $this->assertEquals($updateVehicule, $this->repository->findById(self::VEHICLE_ID));
    }

    public function testDeleteVehicle(): void
    {
        $this->createTestVehicle();
        $this->assertSame(1, count((array) $this->repository->findByUserId(self::USER_ID)));
        $vehicle = $this->repository->findById(self::VEHICLE_ID);
        $this->assertInstanceOf(Vehicle::class, $vehicle);
        $this->repository->delete($vehicle);
        $this->assertEmpty($this->repository->findByUserId(self::USER_ID));
    }

    public function testFindVehiculeByIdReturnNullIfNotFound(): void
    {
        $this->assertNull($this->repository->findById('not-existing-id'));
    }

    public function testFindVehicleById(): void
    {
        $this->createTestVehicle();
        $vehicle = $this->repository->findById(self::VEHICLE_ID);
        $expectedVehicle = new Vehicle(
            self::VEHICLE_ID,
            self::USER_ID,
            self::MAKE,
            self::MODEL,
            new \DateTimeImmutable(self::REGISTRATION_DATE)
        );

        $this->assertEquals($expectedVehicle, $vehicle);
    }

    protected function setUp(): void
    {
        $this->repository = new VehicleInMemoryRepository();
    }

    protected function tearDown(): void
    {
        unset($this->repository);
    }

    private function createTestVehicle(): void
    {
        $vehicle = new Vehicle(
            self::VEHICLE_ID,
            self::USER_ID,
            self::MAKE,
            'Interstar',
            new \DateTimeImmutable(self::REGISTRATION_DATE)
        );

        $this->repository->create($vehicle);
    }
}
