<?php

namespace Tests\_Mock\Repository\User;

use Domain\User\Entity\User;
use Domain\User\Repository\UserRepositoryInterface;

class UserInMemoryRepository implements UserRepositoryInterface
{
    /**
     * @var User[]
     */
    private array $users = [];

    public function findUserByEmail(string $email): ?User
    {
        $find = function (User $user) use ($email) {
            return $user->getEmail() === $email;
        };

        $user = array_values(array_filter($this->users, $find));

        if (empty($user)) {
            return null;
        }

        return $user[0];
    }

    public function findUserById(string $id): ?User
    {
        $find = function (User $user) use ($id) {
            return $user->getId() === $id;
        };

        $user = array_values(array_filter($this->users, $find));

        if (empty($user)) {
            return null;
        }

        return $user[0];
    }

    public function createUser(User $user): User
    {
        $this->users[] = $user;

        return $user;
    }

    public function updateUser(User $user): User
    {
        for ($i = 0; $i < count($this->users); ++$i) {
            if ($this->users[$i]->getId() === $user->getId()) {
                $this->users[$i] = $user;
                break;
            }
        }

        return $user;
    }

    public function deleteUser(User $user): bool
    {
        for ($i = 0; $i < count($this->users); ++$i) {
            if ($this->users[$i]->getId() === $user->getId()) {
                unset($this->users[$i]);

                return true;
            }
        }

        return false;
    }

    public function findAll(): array
    {
        return $this->users;
    }
}
