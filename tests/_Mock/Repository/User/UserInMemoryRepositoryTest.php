<?php

namespace Tests\_Mock\Repository\User;

use Domain\User\Entity\User;
use Domain\User\RoleEnum;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Application\User\Entity\UserBuilder;

class UserInMemoryRepositoryTest extends TestCase
{
    private UserInMemoryRepository $repository;

    public function testCreateUser(): void
    {
        $this->createTestUser();
        $this->assertSame(1, count($this->repository->findAll()));
    }

    public function testUpdateUser(): void
    {
        $this->createTestUser();
        $updateUser = new User(UserBuilder::ID, 'updated_email@test.fr', 'updated_password', RoleEnum::RoleUser);
        $this->repository->updateUser($updateUser);
        $this->assertEquals($updateUser, $this->repository->findAll()[0]);
    }

    public function testDeleteUser(): void
    {
        $this->createTestUser();
        $this->assertSame(1, count($this->repository->findAll()));
        $user = $this->repository->findUserById(UserBuilder::ID);
        $this->assertInstanceOf(User::class, $user);
        $this->repository->deleteUser($user);
        $this->assertEmpty($this->repository->findAll());
    }

    public function testFindUserByEmailReturnNullIfNotFound(): void
    {
        $this->assertNull($this->repository->findUserByEmail('not_existing_email@test.fr'));
    }

    public function testFindUserByEmail(): void
    {
        $this->createTestUser();
        $user = $this->repository->findUserByEmail(UserBuilder::EMAIL);
        $expectedUser = new User(UserBuilder::ID, UserBuilder::EMAIL, UserBuilder::PASSWORD, RoleEnum::RoleUser);
        $this->assertEquals($expectedUser, $user);
    }

    public function testFindUserByIdReturnNullIfNotFound(): void
    {
        $this->assertNull($this->repository->findUserById('not_existing_id'));
    }

    public function testFindUserById(): void
    {
        $this->createTestUser();
        $user = $this->repository->findUserById(UserBuilder::ID);
        $expectedUser = new User(UserBuilder::ID, UserBuilder::EMAIL, UserBuilder::PASSWORD, RoleEnum::RoleUser);
        $this->assertEquals($expectedUser, $user);
    }

    protected function setUp(): void
    {
        $this->repository = new UserInMemoryRepository();
    }

    protected function tearDown(): void
    {
        unset($this->repository);
    }

    private function createTestUser(): void
    {
        $newUser = UserBuilder::build();
        $this->repository->createUser($newUser);
    }
}
