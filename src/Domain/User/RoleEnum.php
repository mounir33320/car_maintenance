<?php

namespace Domain\User;

enum RoleEnum: string
{
    case RoleAdmin = 'admin';
    case RoleUser = 'user';
}
