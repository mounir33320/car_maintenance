<?php

namespace Domain\User\Entity;

use Domain\User\RoleEnum;

class User
{
    public function __construct(
        private readonly string $id,
        private readonly string $email,
        private readonly string $password,
        private readonly RoleEnum $role,
        private readonly bool $isActive = true
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getRole(): RoleEnum
    {
        return $this->role;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }
}
