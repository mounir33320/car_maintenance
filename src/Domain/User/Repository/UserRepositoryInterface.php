<?php

namespace Domain\User\Repository;

use Domain\User\Entity\User;

interface UserRepositoryInterface
{
    /**
     * @return User[]|array
     */
    public function findAll(): array;

    public function findUserByEmail(string $email): ?User;

    public function findUserById(string $id): ?User;

    public function createUser(User $user): User;

    public function updateUser(User $user): User;

    public function deleteUser(User $user): bool;
}
