<?php

namespace Domain\Vehicle\Entity;

use Domain\Vehicle\VehicleFileType;

readonly class VehicleFile
{
    public function __construct(
        private string $id,
        private string $vehicleId,
        private VehicleFileType $vehicleFileType,
        private ?string $fileUrl = null
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getVehicleId(): string
    {
        return $this->vehicleId;
    }

    public function getVehicleFileType(): VehicleFileType
    {
        return $this->vehicleFileType;
    }

    public function getFileUrl(): ?string
    {
        return $this->fileUrl;
    }
}
