<?php

namespace Domain\Vehicle\Entity;

use Domain\Shared\Exception\TooManyFilesException;

class Vehicle
{
    public const MILEAGE_OIL_CHANGE_INTERVAL = 15000;

    /**
     * @var VehicleFile[]
     */
    private array $registrationDocuments = [];

    /**
     * @var VehicleFile[]
     */
    private array $insuranceFiles = [];

    public function __construct(
        private readonly string $id,
        private readonly string $userId,
        private readonly string $make,
        private readonly string $model,
        private readonly \DateTimeImmutable $firstRegistrationDate,
        private readonly string $registrationNumber = '',
        private readonly ?int $purchasePrice = null,
        private readonly ?int $mileageAtPurchase = null,
        private readonly ?int $currentKnownMileage = null,
        private readonly ?\DateTimeImmutable $lastOilChangeDate = null,
        private readonly ?int $lastOilChangeMileage = null,
        private readonly ?int $timingBeltServiceIntervalsInYears = null,
        private readonly ?int $timingBeltServiceIntervalsInMileage = null,
        private readonly ?\DateTimeImmutable $lastTimingBeltChangeDate = null,
        private readonly ?int $lastTimingBeltChangeMileage = null,
        private readonly ?\DateTimeImmutable $lastTechnicalInspection = null,
        private readonly ?\DateTimeImmutable $lastPollutionControl = null
    ) {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function userId(): string
    {
        return $this->userId;
    }

    public function make(): string
    {
        return $this->make;
    }

    public function model(): string
    {
        return $this->model;
    }

    public function firstRegistrationDate(): \DateTimeImmutable
    {
        return $this->firstRegistrationDate;
    }

    public function registrationNumber(): string
    {
        return $this->registrationNumber;
    }

    public function purchasePrice(): ?int
    {
        return $this->purchasePrice;
    }

    public function mileageAtPurchase(): ?int
    {
        return $this->mileageAtPurchase;
    }

    public function currentKnownMileage(): ?int
    {
        return $this->currentKnownMileage;
    }

    public function lastOilChangeDate(): ?\DateTimeImmutable
    {
        return $this->lastOilChangeDate;
    }

    public function lastOilChangeMileage(): ?int
    {
        return $this->lastOilChangeMileage;
    }

    public function timingBeltServiceIntervalsInYears(): ?int
    {
        return $this->timingBeltServiceIntervalsInYears;
    }

    public function timingBeltServiceIntervalsInMileage(): ?int
    {
        return $this->timingBeltServiceIntervalsInMileage;
    }

    public function lastTimingBeltChangeDate(): ?\DateTimeImmutable
    {
        return $this->lastTimingBeltChangeDate;
    }

    public function lastTimingBeltChangeMileage(): ?int
    {
        return $this->lastTimingBeltChangeMileage;
    }

    public function lastTechnicalInspectionDate(): ?\DateTimeImmutable
    {
        return $this->lastTechnicalInspection;
    }

    public function nextTechnicalInspection(): ?\DateTimeImmutable
    {
        return $this->lastTechnicalInspection?->modify('+2 years');
    }

    public function lastPollutionControlDate(): ?\DateTimeImmutable
    {
        return $this->lastPollutionControl;
    }

    public function nextPollutionControl(): ?\DateTimeImmutable
    {
        return $this->lastTechnicalInspection?->modify('+1 years');
    }

    public function nextOilChangeDate(): ?\DateTimeImmutable
    {
        return $this->lastOilChangeDate?->modify('+1 year');
    }

    public function nexOilChangeMileage(): ?int
    {
        if (!$this->lastOilChangeMileage) {
            return null;
        }

        return $this->lastOilChangeMileage + self::MILEAGE_OIL_CHANGE_INTERVAL;
    }

    public function addRegistrationDocument(VehicleFile $registrationFile): void
    {
        if (2 === count($this->registrationDocuments)) {
            throw new TooManyFilesException();
        }

        $this->registrationDocuments[] = $registrationFile;
    }

    public function addInsurance(VehicleFile $insuranceFile): void
    {
        if (2 === count($this->insuranceFiles)) {
            throw new TooManyFilesException();
        }

        $this->insuranceFiles[] = $insuranceFile;
    }

    /**
     * @return VehicleFile[]
     */
    public function insuranceFiles(): array
    {
        return $this->insuranceFiles;
    }

    /**
     * @return VehicleFile[]
     */
    public function registrationDocumentFiles(): array
    {
        return $this->registrationDocuments;
    }

    public function removeFile(string $vehicleFileId): void
    {
        $filterArrayOfFilesById = fn ($vehicleFile) => $vehicleFile->getId() !== $vehicleFileId;

        $this->registrationDocuments = array_filter($this->registrationDocuments, $filterArrayOfFilesById);
        $this->insuranceFiles = array_filter($this->insuranceFiles, $filterArrayOfFilesById);
    }

    public function isFileExistsByFileId(string $vehicleFileId): bool
    {
        $filterArrayOfFilesById = fn ($vehicleFile) => $vehicleFile->getId() === $vehicleFileId;
        $isFileExists = !empty(array_filter($this->registrationDocuments, $filterArrayOfFilesById));

        if ($isFileExists) {
            return true;
        }

        return !empty(array_filter($this->insuranceFiles, $filterArrayOfFilesById));
    }
}
