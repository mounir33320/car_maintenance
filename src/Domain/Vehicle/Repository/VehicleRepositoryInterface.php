<?php

namespace Domain\Vehicle\Repository;

use Domain\Vehicle\Entity\Vehicle;

interface VehicleRepositoryInterface
{
    /**
     * @return Vehicle[]
     */
    public function findByUserId(string $userId): array;

    public function findById(string $id): ?Vehicle;

    public function create(Vehicle $vehicle): void;

    public function update(Vehicle $vehicle): void;

    public function delete(Vehicle $vehicle): void;
}
