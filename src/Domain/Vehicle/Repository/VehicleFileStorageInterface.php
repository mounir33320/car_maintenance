<?php

namespace Domain\Vehicle\Repository;

use Domain\Vehicle\Entity\VehicleFile;

interface VehicleFileStorageInterface
{
    public function store(VehicleFile $file): void;

    public function remove(VehicleFile $file): void;

    // TODO prefix ==> userId/vehicleId/file_type/file_id_1
    public function generateFileUrl(VehicleFile $file): string;
}
