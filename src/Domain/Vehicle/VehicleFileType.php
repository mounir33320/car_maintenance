<?php

namespace Domain\Vehicle;

enum VehicleFileType: string
{
    case REGISTRATION_DOCUMENT = 'registration_document';
    case INSURANCE = 'insurance';
}
