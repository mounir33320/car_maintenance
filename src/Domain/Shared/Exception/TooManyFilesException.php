<?php

namespace Domain\Shared\Exception;

class TooManyFilesException extends \Exception
{
}
