<?php

namespace Infrastructure\Service;

use Application\Service\UUIDGeneratorInterface;
use Symfony\Component\Uid\Uuid;

class UUIDGenerator implements UUIDGeneratorInterface
{
    public function generate(): string
    {
        return Uuid::v4()->toRfc4122();
    }
}
