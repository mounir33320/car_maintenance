<?php

namespace Infrastructure\Service;

use Application\Service\PasswordHasherInterface;
use Application\Service\PasswordVerifierInterface;

class PasswordHasherAndVerifier implements PasswordHasherInterface, PasswordVerifierInterface
{
    public function hash(string $plainPassword): string
    {
        return password_hash($plainPassword, PASSWORD_BCRYPT);
    }

    public function verify(string $plainPassword, string $hashedPassword): bool
    {
        return password_verify($plainPassword, $hashedPassword);
    }
}
