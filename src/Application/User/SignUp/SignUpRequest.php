<?php

namespace Application\User\SignUp;

readonly class SignUpRequest
{
    public function __construct(
        public string $email,
        public string $password,
        public string $role = 'user',
        public bool $isActive = true
    ) {
    }
}
