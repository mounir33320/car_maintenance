<?php

namespace Application\User\SignUp;

use Application\Service\PasswordFormatChecker;
use Application\Service\PasswordHasherInterface;
use Application\Service\UUIDGeneratorInterface;
use Application\Shared\Error\ErrorNotification;
use Application\User\Shared\UserErrorMessageConstants;
use Assert\Assert;
use Assert\LazyAssertionException;
use Domain\User\Entity\User;
use Domain\User\Repository\UserRepositoryInterface;
use Domain\User\RoleEnum;

readonly class SignUp implements SignUpInterface
{
    public function __construct(
        private UserRepositoryInterface $userRepository,
        private PasswordHasherInterface $passwordHasher,
        private UUIDGeneratorInterface $uuidGenerator,
        private PasswordFormatChecker $passwordFormatChecker
    ) {
    }

    public function execute(SignUpRequest $request, SignUpPresenterInterface $presenter): void
    {
        $response = new SignUpResponse(new ErrorNotification());

        $isValid = $this->checkRequest($request, $response);
        $isValid = $isValid && $this->checkExistsUser($request, $response);
        $isValid = $isValid && $this->checkRole($request, $response);
        $isValid = $isValid && $this->checkPasswordFormat($request, $response);

        if ($isValid) {
            $uuid = $this->uuidGenerator->generate();
            $hashedPassword = $this->passwordHasher->hash($request->password);
            $role = RoleEnum::from($request->role);
            $user = new User($uuid, $request->email, $hashedPassword, $role, $request->isActive);
            $createdUser = $this->userRepository->createUser($user);

            $response->setCreatedUser($createdUser);
        }

        $presenter->present($response);
    }

    private function checkPasswordFormat(SignUpRequest $request, SignUpResponse $response): bool
    {
        if (!$this->passwordFormatChecker->check($request->password)) {
            $response->addError('password', UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER);

            return false;
        }

        return true;
    }

    private function checkRequest(SignUpRequest $request, SignUpResponse $response): bool
    {
        try {
            Assert::lazy()
                ->that($request->email, 'email')
                    ->email(UserErrorMessageConstants::INVALID_EMAIL_FORMAT)
                ->verifyNow()
            ;

            return true;
        } catch (LazyAssertionException $exception) {
            foreach ($exception->getErrorExceptions() as $errorException) {
                $response->addError((string) $errorException->getPropertyPath(), $errorException->getMessage());
            }

            return false;
        }
    }

    private function checkExistsUser(SignUpRequest $request, SignUpResponse $response): bool
    {
        $existingUser = $this->userRepository->findUserByEmail($request->email);

        if ($existingUser) {
            $response->addError('email', UserErrorMessageConstants::USER_ALREADY_EXISTS);

            return false;
        }

        return true;
    }

    private function checkRole(SignUpRequest $request, SignUpResponse $response): bool
    {
        if (!RoleEnum::tryFrom($request->role)) {
            $response->addError('role', UserErrorMessageConstants::INVALID_ROLE);

            return false;
        }

        return true;
    }
}
