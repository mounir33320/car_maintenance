<?php

namespace Application\User\SignUp;

interface SignUpInterface
{
    public function execute(SignUpRequest $request, SignUpPresenterInterface $presenter): void;
}
