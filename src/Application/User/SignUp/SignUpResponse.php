<?php

namespace Application\User\SignUp;

use Application\Shared\Error\ErrorNotificationInterface;
use Domain\User\Entity\User;

class SignUpResponse
{
    private ?User $user = null;

    public function __construct(private readonly ErrorNotificationInterface $errorNotification)
    {
    }

    public function addError(string $field, string $message): void
    {
        $this->errorNotification->add($field, $message);
    }

    public function setCreatedUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedUser(): ?User
    {
        return $this->user;
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }
}
