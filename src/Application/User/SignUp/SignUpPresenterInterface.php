<?php

namespace Application\User\SignUp;

interface SignUpPresenterInterface
{
    public function present(SignUpResponse $response): void;
}
