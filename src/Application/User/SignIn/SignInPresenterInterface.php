<?php

namespace Application\User\SignIn;

interface SignInPresenterInterface
{
    public function present(SignInResponse $response): void;
}
