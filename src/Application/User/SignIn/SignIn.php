<?php

namespace Application\User\SignIn;

use Application\Service\PasswordVerifierInterface;
use Application\Shared\Error\ErrorNotification;
use Application\User\Shared\UserErrorMessageConstants;
use Domain\User\Entity\User;
use Domain\User\Repository\UserRepositoryInterface;

class SignIn implements SignInInterface
{
    private ?User $user = null;

    public function __construct(
        readonly private UserRepositoryInterface $userRepository,
        readonly private PasswordVerifierInterface $passwordVerifier,
    ) {
    }

    public function execute(SignInRequest $request, SignInPresenterInterface $presenter): void
    {
        $response = new SignInResponse(new ErrorNotification());

        $isValid = $this->checkExistingUser($request, $response);
        $isValid = $isValid && $this->checkPassword($request, $response);

        if ($isValid && $this->user instanceof User) {
            $response->setUser($this->user);
        }

        $presenter->present($response);
    }

    private function checkExistingUser(SignInRequest $request, SignInResponse $response): bool
    {
        $this->user = $this->userRepository->findUserByEmail($request->email);

        if (!$this->user instanceof User) {
            $response->addError('password', UserErrorMessageConstants::INVALID_CREDENTIALS);

            return false;
        }

        return true;
    }

    private function checkPassword(SignInRequest $request, SignInResponse $response): bool
    {
        if ($this->user) {
            $isValid = $this->passwordVerifier->verify($request->password, $this->user->getPassword());

            if (!$isValid) {
                $response->addError('password', UserErrorMessageConstants::INVALID_CREDENTIALS);

                return false;
            }

            return true;
        }

        return false;
    }
}
