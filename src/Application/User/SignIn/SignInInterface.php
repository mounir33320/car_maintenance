<?php

namespace Application\User\SignIn;

interface SignInInterface
{
    public function execute(SignInRequest $request, SignInPresenterInterface $presenter): void;
}
