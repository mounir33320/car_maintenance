<?php

namespace Application\User\SignIn;

use Application\Shared\Error\ErrorNotificationInterface;
use Domain\User\Entity\User;

class SignInResponse
{
    private ?User $user = null;

    public function __construct(private readonly ErrorNotificationInterface $errorNotification)
    {
    }

    public function addError(string $field, string $message): void
    {
        $this->errorNotification->add($field, $message);
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }
}
