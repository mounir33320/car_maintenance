<?php

namespace Application\User\SignIn;

readonly class SignInRequest
{
    public function __construct(public string $email, public string $password)
    {
    }
}
