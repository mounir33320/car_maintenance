<?php

namespace Application\User\Shared;

class UserErrorMessageConstants
{
    public const USER_ALREADY_EXISTS = 'user_already_exists';
    public const USER_CANNOT_UPDATE_ANOTHER_USER = 'user_cannot_update_another_user';
    public const INVALID_EMAIL_FORMAT = 'invalid_email_format';
    public const INVALID_ROLE = 'invalid_role';
    public const USER_NOT_FOUND = 'user_not_found';
    public const PASSWORD_CANNOT_BE_EMPTY = 'password_cannot_be_empty';
    public const PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER = 'password_must_contains_lowercase_letter_uppercase_letter_number_special_character';
    public const INVALID_CREDENTIALS = 'invalid_credentials';
    public const WRONG_PASSWORD = 'wrong_password';
}
