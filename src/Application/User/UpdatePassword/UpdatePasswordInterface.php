<?php

namespace Application\User\UpdatePassword;

interface UpdatePasswordInterface
{
    public function execute(UpdatePasswordRequest $request, UpdatePasswordPresenterInterface $presenter): void;
}
