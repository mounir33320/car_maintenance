<?php

namespace Application\User\UpdatePassword;

readonly class UpdatePasswordRequest
{
    public function __construct(public string $currentUserId, public string $userToUpdateId, public string $oldPlainPassword, public string $newPlainPassword)
    {
    }
}
