<?php

namespace Application\User\UpdatePassword;

use Application\Service\PasswordFormatChecker;
use Application\Service\PasswordHasherInterface;
use Application\Service\PasswordVerifierInterface;
use Application\Shared\Error\ErrorNotification;
use Application\User\Shared\UserErrorMessageConstants;
use Domain\User\Entity\User;
use Domain\User\Repository\UserRepositoryInterface;

class UpdatePassword implements UpdatePasswordInterface
{
    private ?User $userToUpdate = null;

    public function __construct(
        private readonly UserRepositoryInterface $userRepository,
        private readonly PasswordVerifierInterface $passwordVerifier,
        private readonly PasswordHasherInterface $passwordHasher,
        private readonly PasswordFormatChecker $passwordFormatChecker
    ) {
    }

    public function execute(UpdatePasswordRequest $request, UpdatePasswordPresenterInterface $presenter): void
    {
        $response = new UpdatePasswordResponse(new ErrorNotification());

        $isValid = $this->checkUserExists($request, $response);
        $isValid = $isValid && $this->checkCurrentUser($request, $response);
        $isValid = $isValid && $this->checkPasswordFormat($request, $response);
        $isValid = $isValid && $this->checkOldPassword($request, $response);

        if ($isValid && $this->userToUpdate) {
            $hashedNewPassword = $this->passwordHasher->hash($request->newPlainPassword);
            $updatedUser = new User(
                $this->userToUpdate->getId(),
                $this->userToUpdate->getEmail(),
                $hashedNewPassword,
                $this->userToUpdate->getRole(),
                $this->userToUpdate->isActive()
            );

            $this->userRepository->updateUser($updatedUser);
            $response->setUpdatedUser($updatedUser);
        }

        $presenter->present($response);
    }

    private function checkUserExists(UpdatePasswordRequest $request, UpdatePasswordResponse $response): bool
    {
        $this->userToUpdate = $this->userRepository->findUserById($request->userToUpdateId);

        if (!$this->userToUpdate) {
            $response->addError('id', UserErrorMessageConstants::USER_NOT_FOUND);

            return false;
        }

        return true;
    }

    private function checkCurrentUser(UpdatePasswordRequest $request, UpdatePasswordResponse $response): bool
    {
        if ($this->userToUpdate && $this->userToUpdate->getId() !== $request->currentUserId) {
            $response->addError('id', UserErrorMessageConstants::USER_CANNOT_UPDATE_ANOTHER_USER);

            return false;
        }

        return true;
    }

    private function checkPasswordFormat(UpdatePasswordRequest $request, UpdatePasswordResponse $response): bool
    {
        if (!$this->passwordFormatChecker->check($request->newPlainPassword)) {
            $response->addError('password', UserErrorMessageConstants::PASSWORD_MUST_CONTAINS_LOWERCASE_LETTER_UPPERCASE_LETTER_NUMBER_SPECIAL_CHARACTER);

            return false;
        }

        return true;
    }

    private function checkOldPassword(UpdatePasswordRequest $request, UpdatePasswordResponse $response): bool
    {
        if ($this->userToUpdate && !$this->passwordVerifier->verify($request->oldPlainPassword, $this->userToUpdate->getPassword())) {
            $response->addError('password', UserErrorMessageConstants::WRONG_PASSWORD);

            return false;
        }

        return true;
    }
}
