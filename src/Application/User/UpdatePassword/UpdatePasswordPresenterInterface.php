<?php

namespace Application\User\UpdatePassword;

interface UpdatePasswordPresenterInterface
{
    public function present(UpdatePasswordResponse $response): void;
}
