<?php

namespace Application\User\UpdatePassword;

use Application\Shared\Error\ErrorNotificationInterface;
use Domain\User\Entity\User;

class UpdatePasswordResponse
{
    private ?User $updatedUser = null;

    public function __construct(private readonly ErrorNotificationInterface $errorNotification)
    {
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }

    public function addError(string $field, string $message): void
    {
        $this->errorNotification->add($field, $message);
    }

    public function getUpdatedUser(): ?User
    {
        return $this->updatedUser;
    }

    public function setUpdatedUser(User $user): void
    {
        $this->updatedUser = $user;
    }
}
