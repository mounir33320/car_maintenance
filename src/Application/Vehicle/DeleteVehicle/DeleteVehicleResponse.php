<?php

namespace Application\Vehicle\DeleteVehicle;

use Application\Shared\Error\ErrorNotificationInterface;

class DeleteVehicleResponse
{
    private bool $isVehicleDeleted = false;

    public function __construct(private readonly ErrorNotificationInterface $errorNotification)
    {
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }

    public function addError(string $field, string $message): void
    {
        $this->errorNotification->add($field, $message);
    }

    public function isVehicleDeleted(): bool
    {
        return $this->isVehicleDeleted;
    }

    public function setIsVehicleDeleted(bool $isVehicleDeleted): void
    {
        $this->isVehicleDeleted = $isVehicleDeleted;
    }
}
