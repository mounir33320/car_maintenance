<?php

namespace Application\Vehicle\DeleteVehicle;

use Application\Shared\Error\ErrorNotification;
use Application\User\Shared\UserErrorMessageConstants;
use Application\Vehicle\Shared\VehicleErrorMessageConstants;
use Domain\User\Repository\UserRepositoryInterface;
use Domain\Vehicle\Repository\VehicleRepositoryInterface;

readonly class DeleteVehicle implements DeleteVehicleInterface
{
    public function __construct(
        private VehicleRepositoryInterface $vehicleRepository,
        private UserRepositoryInterface $userRepository
    ) {
    }

    public function execute(DeleteVehicleRequest $request, DeleteVehiclePresenterInterface $presenter): void
    {
        $response = new DeleteVehicleResponse(new ErrorNotification());

        $isValid = $this->isExistingUser($request, $response);
        $isValid = $isValid && $this->isVehicleExists($request, $response);

        if ($isValid) {
            $vehicle = $this->vehicleRepository->findById($request->vehicleId);
            if ($vehicle) {
                $this->vehicleRepository->delete($vehicle);
                $response->setIsVehicleDeleted(true);
            }
        }

        $presenter->present($response);
    }

    private function isExistingUser(DeleteVehicleRequest $request, DeleteVehicleResponse $response): bool
    {
        $user = $this->userRepository->findUserById($request->userId);

        if (!$user) {
            $response->addError('userId', UserErrorMessageConstants::USER_NOT_FOUND);

            return false;
        }

        return true;
    }

    private function isVehicleExists(DeleteVehicleRequest $request, DeleteVehicleResponse $response): bool
    {
        $vehicle = $this->vehicleRepository->findById($request->vehicleId);

        if (!$vehicle) {
            $response->addError('id', VehicleErrorMessageConstants::VEHICLE_NOT_FOUND);

            return false;
        }

        return true;
    }
}
