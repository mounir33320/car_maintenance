<?php

namespace Application\Vehicle\DeleteVehicle;

interface DeleteVehiclePresenterInterface
{
    public function present(DeleteVehicleResponse $response): void;
}
