<?php

namespace Application\Vehicle\DeleteVehicle;

interface DeleteVehicleInterface
{
    public function execute(DeleteVehicleRequest $request, DeleteVehiclePresenterInterface $presenter): void;
}
