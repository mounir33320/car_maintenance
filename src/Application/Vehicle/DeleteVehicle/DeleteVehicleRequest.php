<?php

namespace Application\Vehicle\DeleteVehicle;

readonly class DeleteVehicleRequest
{
    public function __construct(public string $vehicleId, public string $userId)
    {
    }
}
