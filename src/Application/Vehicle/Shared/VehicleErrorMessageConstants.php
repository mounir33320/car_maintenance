<?php

namespace Application\Vehicle\Shared;

class VehicleErrorMessageConstants
{
    public const INVALID_DATE_FORMAT = 'invalid_date_format';
    public const INVALID_REGISTRATION_NUMBER = 'invalid_registration_number';
    public const CANNOT_BE_NEGATIVE = 'cannot_be_negative';
    public const TOO_LARGE_NUMBER = 'too_large_number';
    public const VEHICLE_NOT_FOUND = 'vehicle_not_found';
}
