<?php

namespace Application\Vehicle\Shared;

class VehicleFileErrorMessageConstants
{
    public const TOO_LARGE_FILE = 'too_large_file';

    public const INCORRECT_VEHICLE_FILE_TYPE = 'incorrect_vehicle_file_type';
    public const INCORRECT_FILE_EXTENSION = 'incorrect_file_extension';
    public const INCORRECT_MIME_TYPE = 'incorrect_mime_type';

    public const TOO_MANY_FILES = 'too_many_files';
    public const VEHICLE_FILE_NOT_FOUND = 'vehicle_file_not_found';
}
