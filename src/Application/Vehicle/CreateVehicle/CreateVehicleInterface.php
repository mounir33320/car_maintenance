<?php

namespace Application\Vehicle\CreateVehicle;

interface CreateVehicleInterface
{
    public function execute(CreateVehicleRequest $request, CreateVehiclePresenterInterface $presenter): void;
}
