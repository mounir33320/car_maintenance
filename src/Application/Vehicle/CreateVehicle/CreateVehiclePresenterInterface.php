<?php

namespace Application\Vehicle\CreateVehicle;

interface CreateVehiclePresenterInterface
{
    public function present(CreateVehicleResponse $response): void;
}
