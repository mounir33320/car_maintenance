<?php

namespace Application\Vehicle\CreateVehicle;

use Application\Shared\Error\ErrorNotificationInterface;
use Domain\Vehicle\Entity\Vehicle;

class CreateVehicleResponse
{
    private ?Vehicle $vehicle = null;

    public function __construct(private readonly ErrorNotificationInterface $errorNotification)
    {
    }

    public function addError(string $field, string $message): void
    {
        $this->errorNotification->add($field, $message);
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }

    public function getVehicle(): ?Vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(Vehicle $vehicle): void
    {
        $this->vehicle = $vehicle;
    }
}
