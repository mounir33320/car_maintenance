<?php

namespace Application\Vehicle\CreateVehicle;

use Application\Service\UUIDGeneratorInterface;
use Application\Shared\Error\ErrorNotification;
use Application\User\Shared\UserErrorMessageConstants;
use Application\Vehicle\Shared\VehicleErrorMessageConstants;
use Assert\Assert;
use Assert\LazyAssertionException;
use Domain\User\Repository\UserRepositoryInterface;
use Domain\Vehicle\Entity\Vehicle;
use Domain\Vehicle\Repository\VehicleRepositoryInterface;

class CreateVehicle implements CreateVehicleInterface
{
    public function __construct(
        private readonly VehicleRepositoryInterface $vehicleRepository,
        private readonly UserRepositoryInterface $userRepository,
        private readonly UUIDGeneratorInterface $uuidGenerator
    ) {
    }

    public function execute(CreateVehicleRequest $request, CreateVehiclePresenterInterface $presenter): void
    {
        $response = new CreateVehicleResponse(new ErrorNotification());

        $isValid = $this->checkUser($request, $response);
        $isValid = $isValid && $this->checkRequest($request, $response);

        if ($isValid) {
            $vehicle = new Vehicle(
                $this->uuidGenerator->generate(),
                $request->userId,
                $request->make,
                $request->model,
                new \DateTimeImmutable($request->firstRegistrationDateISO),
                $request->registrationNumber,
                $request->purchasePrice,
                $request->mileageAtPurchase,
                $request->currentKnownMileage,
                $request->lastOilChangeDateISO ? new \DateTimeImmutable($request->lastOilChangeDateISO) : null,
                $request->lastOilChangeMileage,
                $request->timingBeltServiceIntervalsInYears,
                $request->timingBeltServiceIntervalsInMileage,
                $request->lastTimingBeltChangeDateISO ? new \DateTimeImmutable($request->lastTimingBeltChangeDateISO) : null,
                $request->lastTimingBeltChangeMileage,
                $request->lastTechnicalInspectionDateISO ? new \DateTimeImmutable($request->lastTechnicalInspectionDateISO) : null,
                $request->lastPollutionControlDateISO ? new \DateTimeImmutable($request->lastPollutionControlDateISO) : null
            );

            $this->vehicleRepository->create($vehicle);

            $response->setVehicle($vehicle);
        }

        $presenter->present($response);
    }

    private function checkUser(CreateVehicleRequest $request, CreateVehicleResponse $response): bool
    {
        $user = $this->userRepository->findUserById($request->userId);

        if (!$user) {
            $response->addError('userId', UserErrorMessageConstants::USER_NOT_FOUND);

            return false;
        }

        return true;
    }

    private function checkRequest(CreateVehicleRequest $request, CreateVehicleResponse $response): bool
    {
        try {
            $regexValidDate = '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/';
            $regexValidRegistrationNumber = '/^[A-Z]{2}( ?|-?)[0-9]{3}( ?|-?)[A-Z]{2}$|^[0-9]{3,4}( ?|-?)[A-Z]{2,3}( ?|-?)[0-9]{2}$/';

            Assert::lazy()
                ->that($request->firstRegistrationDateISO, 'firstRegistrationDate')
                    ->regex($regexValidDate, VehicleErrorMessageConstants::INVALID_DATE_FORMAT)

                ->that($request->lastOilChangeDateISO, 'lastOilChangeDate')
                    ->regex($regexValidDate, VehicleErrorMessageConstants::INVALID_DATE_FORMAT)

                ->that($request->lastTimingBeltChangeDateISO, 'lastTimingBeltChangeDate')
                    ->regex($regexValidDate, VehicleErrorMessageConstants::INVALID_DATE_FORMAT)

                ->that($request->lastTechnicalInspectionDateISO, 'lastTechnicalInspection')
                    ->regex($regexValidDate, VehicleErrorMessageConstants::INVALID_DATE_FORMAT)

                ->that($request->lastPollutionControlDateISO, 'lastPollutionControl')
                    ->regex($regexValidDate, VehicleErrorMessageConstants::INVALID_DATE_FORMAT)

                ->that($request->registrationNumber, 'registrationNumber')
                    ->regex($regexValidRegistrationNumber, VehicleErrorMessageConstants::INVALID_REGISTRATION_NUMBER)

                ->that($request->purchasePrice, 'purchasePrice')
                    ->min(0, VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE)
                    ->max(100000000, VehicleErrorMessageConstants::TOO_LARGE_NUMBER)

                ->that($request->currentKnownMileage, 'currentKnownMileage')
                    ->min(0, VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE)
                    ->max(100000000, VehicleErrorMessageConstants::TOO_LARGE_NUMBER)

                ->that($request->lastOilChangeMileage, 'lastOilChangeMileage')
                    ->min(0, VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE)
                    ->max(100000000, VehicleErrorMessageConstants::TOO_LARGE_NUMBER)

                ->that($request->timingBeltServiceIntervalsInYears, 'timingBeltServiceIntervalsInYears')
                    ->min(0, VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE)
                    ->max(30, VehicleErrorMessageConstants::TOO_LARGE_NUMBER)
                ->that($request->timingBeltServiceIntervalsInMileage, 'timingBeltServiceIntervalsInMileage')
                    ->min(0, VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE)
                    ->max(500000, VehicleErrorMessageConstants::TOO_LARGE_NUMBER)
                ->that($request->lastTimingBeltChangeMileage, 'lastTimingBeltChangeMileage')
                    ->min(0, VehicleErrorMessageConstants::CANNOT_BE_NEGATIVE)
                    ->max(500000, VehicleErrorMessageConstants::TOO_LARGE_NUMBER)

                ->verifyNow();

            return true;
        } catch (LazyAssertionException $exception) {
            foreach ($exception->getErrorExceptions() as $errorException) {
                $response->addError((string) $errorException->getPropertyPath(), $errorException->getMessage());
            }

            return false;
        }
    }
}
