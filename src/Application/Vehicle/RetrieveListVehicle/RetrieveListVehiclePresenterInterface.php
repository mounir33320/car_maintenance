<?php

namespace Application\Vehicle\RetrieveListVehicle;

interface RetrieveListVehiclePresenterInterface
{
    public function present(RetrieveListVehicleResponse $response): void;
}
