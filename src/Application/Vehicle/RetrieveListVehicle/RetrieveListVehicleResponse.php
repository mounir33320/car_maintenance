<?php

namespace Application\Vehicle\RetrieveListVehicle;

use Application\Shared\Error\ErrorNotificationInterface;
use Domain\Vehicle\Entity\Vehicle;

class RetrieveListVehicleResponse
{
    /**
     * @var Vehicle[]|array
     */
    private array $listOfVehicles = [];

    public function __construct(private readonly ErrorNotificationInterface $errorNotification)
    {
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }

    public function addError(string $field, string $message): void
    {
        $this->errorNotification->add($field, $message);
    }

    /**
     * @param Vehicle[]|array $listOfVehicles
     */
    public function setListOfVehicles(array $listOfVehicles): void
    {
        $this->listOfVehicles = $listOfVehicles;
    }

    /**
     * @return Vehicle[]|array
     */
    public function getListOfVehicles(): array
    {
        return $this->listOfVehicles;
    }
}
