<?php

namespace Application\Vehicle\RetrieveListVehicle;

interface RetrieveListVehicleInterface
{
    public function execute(RetrieveListVehicleRequest $request, RetrieveListVehiclePresenterInterface $presenter): void;
}
