<?php

namespace Application\Vehicle\RetrieveListVehicle;

readonly class RetrieveListVehicleRequest
{
    public function __construct(public string $currentUserId)
    {
    }
}
