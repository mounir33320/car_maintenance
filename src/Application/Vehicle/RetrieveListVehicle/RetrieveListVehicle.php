<?php

namespace Application\Vehicle\RetrieveListVehicle;

use Application\Shared\Error\ErrorNotification;
use Domain\Vehicle\Repository\VehicleRepositoryInterface;

readonly class RetrieveListVehicle implements RetrieveListVehicleInterface
{
    public function __construct(private VehicleRepositoryInterface $vehicleRepository)
    {
    }

    public function execute(RetrieveListVehicleRequest $request, RetrieveListVehiclePresenterInterface $presenter): void
    {
        $response = new RetrieveListVehicleResponse(new ErrorNotification());

        $listOfVehicles = $this->vehicleRepository->findByUserId($request->currentUserId);
        $response->setListOfVehicles($listOfVehicles);

        $presenter->present($response);
    }
}
