<?php

namespace Application\Vehicle\UpdateVehicle;

interface UpdateVehicleInterface
{
    public function execute(UpdateVehicleRequest $request, UpdateVehiclePresenterInterface $presenter): void;
}
