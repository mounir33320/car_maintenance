<?php

namespace Application\Vehicle\UpdateVehicle;

interface UpdateVehiclePresenterInterface
{
    public function present(UpdateVehicleResponse $response): void;
}
