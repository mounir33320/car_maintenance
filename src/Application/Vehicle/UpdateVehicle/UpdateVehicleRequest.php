<?php

namespace Application\Vehicle\UpdateVehicle;

class UpdateVehicleRequest
{
    public function __construct(
        public string $vehicleId,
        public string $userId,
        public string $make,
        public string $model,
        public string $firstRegistrationDateISO,
        public string $registrationNumber = '',
        public ?int $purchasePrice = null,
        public ?int $mileageAtPurchase = null,
        public ?int $currentKnownMileage = null,
        public ?string $lastOilChangeDateISO = null,
        public ?int $lastOilChangeMileage = null,
        public ?int $timingBeltServiceIntervalsInYears = null,
        public ?int $timingBeltServiceIntervalsInMileage = null,
        public ?string $lastTimingBeltChangeISO = null,
        public ?int $lastTimingBeltChangeMileage = null,
        public ?string $lastTechnicalInspectionISO = null,
        public ?string $lastPollutionControlISO = null
    ) {
    }
}
