<?php

namespace Application\Vehicle\UpdateVehicle;

use Application\Shared\Error\ErrorNotificationInterface;
use Domain\Vehicle\Entity\Vehicle;

class UpdateVehicleResponse
{
    private ?Vehicle $vehicle = null;

    public function __construct(private readonly ErrorNotificationInterface $errorNotification)
    {
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }

    public function addError(string $field, string $message): void
    {
        $this->errorNotification->add($field, $message);
    }

    public function setUpdatedVehicle(Vehicle $vehicle): void
    {
        $this->vehicle = $vehicle;
    }

    public function getUpdatedVehicle(): ?Vehicle
    {
        return $this->vehicle;
    }
}
