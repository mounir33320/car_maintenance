<?php

namespace Application\Vehicle\RetrieveSingleVehicle;

interface RetrieveSingleVehiclePresenterInterface
{
    public function present(RetrieveSingleVehicleResponse $response): void;
}
