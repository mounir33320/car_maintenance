<?php

namespace Application\Vehicle\RetrieveSingleVehicle;

use Application\Shared\Error\ErrorNotification;
use Application\Vehicle\Shared\VehicleErrorMessageConstants;
use Domain\Vehicle\Entity\Vehicle;
use Domain\Vehicle\Repository\VehicleRepositoryInterface;

readonly class RetrieveSingleVehicle implements RetrieveSingleVehicleInterface
{
    public function __construct(private VehicleRepositoryInterface $vehicleRepository)
    {
    }

    public function execute(
        RetrieveSingleVehicleRequest $request,
        RetrieveSingleVehiclePresenterInterface $presenter
    ): void {
        $response = new RetrieveSingleVehicleResponse(new ErrorNotification());

        $vehicle = $this->vehicleRepository->findById($request->vehicleId);
        $isValid = $this->checkVehicle($vehicle, $response);
        $isValid = $isValid && $this->checkUser($vehicle, $request, $response);

        if ($isValid && $vehicle instanceof Vehicle) {
            $response->setVehicle($vehicle);
        }

        $presenter->present($response);
    }

    private function checkVehicle(?Vehicle $vehicle, RetrieveSingleVehicleResponse $response): bool
    {
        if (!$vehicle) {
            $response->addError('id', VehicleErrorMessageConstants::VEHICLE_NOT_FOUND);

            return false;
        }

        return true;
    }

    private function checkUser(
        ?Vehicle $vehicle,
        RetrieveSingleVehicleRequest $request,
        RetrieveSingleVehicleResponse $response
    ): bool {
        if ($vehicle instanceof Vehicle && $vehicle->userId() !== $request->currentUserId) {
            $response->addError('id', VehicleErrorMessageConstants::VEHICLE_NOT_FOUND);

            return false;
        }

        return true;
    }
}
