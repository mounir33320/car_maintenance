<?php

namespace Application\Vehicle\RetrieveSingleVehicle;

readonly class RetrieveSingleVehicleRequest
{
    public function __construct(public string $currentUserId, public string $vehicleId)
    {
    }
}
