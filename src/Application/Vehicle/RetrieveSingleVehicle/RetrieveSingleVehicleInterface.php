<?php

namespace Application\Vehicle\RetrieveSingleVehicle;

interface RetrieveSingleVehicleInterface
{
    public function execute(
        RetrieveSingleVehicleRequest $request,
        RetrieveSingleVehiclePresenterInterface $presenter
    ): void;
}
