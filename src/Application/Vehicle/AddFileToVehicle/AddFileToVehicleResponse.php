<?php

namespace Application\Vehicle\AddFileToVehicle;

use Application\Shared\Error\ErrorNotificationInterface;
use Domain\Vehicle\Entity\VehicleFile;

class AddFileToVehicleResponse
{
    private ?VehicleFile $file = null;

    public function __construct(private readonly ErrorNotificationInterface $errorNotification)
    {
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }

    public function addError(string $field, string $message): void
    {
        $this->errorNotification->add($field, $message);
    }

    public function setFile(VehicleFile $file): void
    {
        $this->file = $file;
    }

    public function getFile(): ?VehicleFile
    {
        return $this->file;
    }
}
