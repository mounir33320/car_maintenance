<?php

namespace Application\Vehicle\AddFileToVehicle;

interface AddFileToVehicleInterface
{
    public function execute(AddFileToVehicleRequest $request, AddFileToVehiclePresenterInterface $presenter): void;
}
