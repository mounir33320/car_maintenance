<?php

namespace Application\Vehicle\AddFileToVehicle;

use Application\Shared\Error\ErrorNotification;
use Application\User\Shared\UserErrorMessageConstants;
use Application\Vehicle\Shared\VehicleErrorMessageConstants;
use Application\Vehicle\Shared\VehicleFileErrorMessageConstants;
use Assert\Assert;
use Assert\LazyAssertionException;
use Domain\Shared\Exception\TooManyFilesException;
use Domain\User\Repository\UserRepositoryInterface;
use Domain\Vehicle\Entity\Vehicle;
use Domain\Vehicle\Entity\VehicleFile;
use Domain\Vehicle\Repository\VehicleFileStorageInterface;
use Domain\Vehicle\Repository\VehicleRepositoryInterface;
use Domain\Vehicle\VehicleFileType;

class AddFileToVehicle implements AddFileToVehicleInterface
{
    private const MAX_FILESIZE = 10 * 1024 * 1024;
    private ?Vehicle $vehicle = null;

    public function __construct(
        private readonly VehicleRepositoryInterface $vehicleRepository,
        private readonly VehicleFileStorageInterface $fileStorage,
        private readonly UserRepositoryInterface $userRepository
    ) {
    }

    public function execute(AddFileToVehicleRequest $request, AddFileToVehiclePresenterInterface $presenter): void
    {
        $response = new AddFileToVehicleResponse(new ErrorNotification());

        $isValid = $this->checkIfVehicleExists($request, $response);
        $isValid = $isValid && $this->checkUser($request, $response);
        $isValid = $isValid && $this->checkFile($request, $response);

        if ($isValid) {
            $file = new VehicleFile(
                'id',
                $request->vehicleId,
                VehicleFileType::from($request->type)
            );

            try {
                $this->addFileToVehicle($file);

                if ($this->vehicle instanceof Vehicle) {
                    $this->vehicleRepository->update($this->vehicle);
                    $this->fileStorage->store($file);
                    $response->setFile($file);
                }
            } catch (TooManyFilesException $exception) {
                $response->addError('id', VehicleFileErrorMessageConstants::TOO_MANY_FILES);
            }
        }

        $presenter->present($response);
    }

    private function checkUser(AddFileToVehicleRequest $request, AddFileToVehicleResponse $response): bool
    {
        $user = $this->userRepository->findUserById($request->userId);
        if (!$user) {
            $response->addError('userId', UserErrorMessageConstants::USER_NOT_FOUND);

            return false;
        }

        return true;
    }

    private function checkIfVehicleExists(AddFileToVehicleRequest $request, AddFileToVehicleResponse $response): bool
    {
        $this->vehicle = $this->vehicleRepository->findById($request->vehicleId);

        if (!$this->vehicle) {
            $response->addError('vehicleId', VehicleErrorMessageConstants::VEHICLE_NOT_FOUND);

            return false;
        }

        return true;
    }

    private function checkFile(AddFileToVehicleRequest $request, AddFileToVehicleResponse $response): bool
    {
        try {
            Assert::lazy()
                ->that($request->fileSize, 'fileSize')
                   ->max(self::MAX_FILESIZE, VehicleFileErrorMessageConstants::TOO_LARGE_FILE)
                ->that($request->type, 'vehicleFileType')
                   ->choice(
                       [VehicleFileType::REGISTRATION_DOCUMENT->value, VehicleFileType::INSURANCE->value],
                       VehicleFileErrorMessageConstants::INCORRECT_VEHICLE_FILE_TYPE
                   )
                ->that($this->extractExtensionFromOriginalName($request->originalName), 'fileExtension')
                   ->choice(
                       ['pdf', 'jpeg', 'jpg', 'png', 'webp'],
                       VehicleFileErrorMessageConstants::INCORRECT_FILE_EXTENSION
                   )
                ->that($request->mimeType, 'mimeType')
                   ->choice(
                       ['image/png', 'image/jpeg', 'image/webp', 'application/pdf'],
                       VehicleFileErrorMessageConstants::INCORRECT_MIME_TYPE
                   )
                ->verifyNow();

            return true;
        } catch (LazyAssertionException $exception) {
            foreach ($exception->getErrorExceptions() as $errorException) {
                $response->addError((string) $errorException->getPropertyPath(), $errorException->getMessage());
            }

            return false;
        }
    }

    private function extractExtensionFromOriginalName(string $originalName): string
    {
        $explodedOriginalName = explode('.', $originalName);

        return array_pop($explodedOriginalName);
    }

    /**
     * @throws TooManyFilesException
     */
    private function addFileToVehicle(VehicleFile $file): void
    {
        if ($this->vehicle) {
            switch ($file->getVehicleFileType()->value) {
                case VehicleFileType::INSURANCE->value:
                    $this->vehicle->addInsurance($file);
                    break;

                case VehicleFileType::REGISTRATION_DOCUMENT->value:
                    $this->vehicle->addRegistrationDocument($file);
                    break;
            }
        }
    }
}
