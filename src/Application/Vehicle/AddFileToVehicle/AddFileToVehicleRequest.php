<?php

namespace Application\Vehicle\AddFileToVehicle;

readonly class AddFileToVehicleRequest
{
    public function __construct(
        public string $vehicleId,
        public string $userId,
        public string $originalName,
        public string $filePath,
        public string $type,
        public int $fileSize,
        public string $mimeType
    ) {
    }
}
