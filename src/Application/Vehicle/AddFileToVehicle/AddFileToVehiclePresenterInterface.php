<?php

namespace Application\Vehicle\AddFileToVehicle;

interface AddFileToVehiclePresenterInterface
{
    public function present(AddFileToVehicleResponse $response): void;
}
