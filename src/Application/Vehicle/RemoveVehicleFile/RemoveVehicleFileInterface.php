<?php

namespace Application\Vehicle\RemoveVehicleFile;

interface RemoveVehicleFileInterface
{
    public function execute(RemoveVehicleFileRequest $request, RemoveVehicleFilePresenterInterface $presenter): void;
}
