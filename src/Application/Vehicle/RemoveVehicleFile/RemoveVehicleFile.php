<?php

namespace Application\Vehicle\RemoveVehicleFile;

use Application\Vehicle\Shared\VehicleErrorMessageConstants;
use Application\Vehicle\Shared\VehicleFileErrorMessageConstants;
use Domain\Vehicle\Entity\Vehicle;
use Domain\Vehicle\Repository\VehicleRepositoryInterface;

class RemoveVehicleFile implements RemoveVehicleFileInterface
{
    private ?Vehicle $vehicle = null;

    public function __construct(private readonly VehicleRepositoryInterface $vehicleRepository)
    {
    }

    public function execute(RemoveVehicleFileRequest $request, RemoveVehicleFilePresenterInterface $presenter): void
    {
        $response = new RemoveVehicleFileResponse();

        $isValid = $this->checkVehicle($request, $response);
        $isValid = $isValid && $this->checkVehicleFile($request, $response);

        if ($isValid && $this->vehicle) {
            $this->vehicle->removeFile($request->vehicleFileId);
            $response->setIsFileWasDeleted(true);
        }

        $presenter->present($response);
    }

    private function checkVehicle(RemoveVehicleFileRequest $request, RemoveVehicleFileResponse $response): bool
    {
        $this->vehicle = $this->vehicleRepository->findById($request->vehicleId);
        if (!$this->vehicle) {
            $response->addError('vehicleId', VehicleErrorMessageConstants::VEHICLE_NOT_FOUND);

            return false;
        }

        return true;
    }

    private function checkVehicleFile(RemoveVehicleFileRequest $request, RemoveVehicleFileResponse $response): bool
    {
        if ($this->vehicle) {
            $isFileExists = $this->vehicle->isFileExistsByFileId($request->vehicleFileId);
            if (!$isFileExists) {
                $response->addError('vehicleFileId', VehicleFileErrorMessageConstants::VEHICLE_FILE_NOT_FOUND);

                return false;
            }

            return true;
        }

        return false;
    }
}
