<?php

namespace Application\Vehicle\RemoveVehicleFile;

interface RemoveVehicleFilePresenterInterface
{
    public function present(RemoveVehicleFileResponse $response): void;
}
