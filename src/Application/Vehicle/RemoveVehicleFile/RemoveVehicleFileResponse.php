<?php

namespace Application\Vehicle\RemoveVehicleFile;

use Application\Shared\Error\ErrorNotification;
use Application\Shared\Error\ErrorNotificationInterface;

class RemoveVehicleFileResponse
{
    private ErrorNotificationInterface $errorNotification;
    private bool $isFileWasDeleted = false;

    public function __construct()
    {
        $this->errorNotification = new ErrorNotification();
    }

    public function getErrorNotification(): ErrorNotificationInterface
    {
        return $this->errorNotification;
    }

    public function addError(string $field, string $message): void
    {
        $this->errorNotification->add($field, $message);
    }

    public function setIsFileWasDeleted(bool $isFileWasDeleted): void
    {
        $this->isFileWasDeleted = $isFileWasDeleted;
    }

    public function isFileWasDeleted(): bool
    {
        return $this->isFileWasDeleted;
    }
}
