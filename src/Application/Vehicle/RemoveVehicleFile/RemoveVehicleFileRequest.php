<?php

namespace Application\Vehicle\RemoveVehicleFile;

readonly class RemoveVehicleFileRequest
{
    public function __construct(public string $vehicleId, public string $vehicleFileId)
    {
    }
}
