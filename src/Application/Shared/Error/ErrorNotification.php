<?php

namespace Application\Shared\Error;

class ErrorNotification implements ErrorNotificationInterface
{
    /**
     * @var Error[]|array
     */
    private array $errors = [];

    public function add(string $fieldId, string $message): self
    {
        $this->errors[] = new Error($fieldId, $message);

        return $this;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function hasError(): bool
    {
        return count($this->errors) > 0;
    }
}
