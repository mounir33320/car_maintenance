<?php

namespace Application\Shared\Error;

readonly class Error
{
    public function __construct(private string $fieldName, private string $message)
    {
    }

    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function __toString()
    {
        return "$this->fieldName:$this->message";
    }
}
