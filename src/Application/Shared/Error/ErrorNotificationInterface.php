<?php

namespace Application\Shared\Error;

interface ErrorNotificationInterface
{
    public function add(string $fieldId, string $message): self;

    /**
     * @return Error[]
     */
    public function getErrors(): array;

    public function hasError(): bool;
}
