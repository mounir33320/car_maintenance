<?php

namespace Application\Service;

interface PasswordHasherInterface
{
    public function hash(string $plainPassword): string;
}
