<?php

namespace Application\Service;

interface PasswordVerifierInterface
{
    public function verify(string $plainPassword, string $hashedPassword): bool;
}
