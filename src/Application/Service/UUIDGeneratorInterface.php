<?php

namespace Application\Service;

interface UUIDGeneratorInterface
{
    public function generate(): string;
}
