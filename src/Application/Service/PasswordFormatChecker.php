<?php

namespace Application\Service;

class PasswordFormatChecker
{
    public function check(string $password): bool
    {
        $pattern = '/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[:!;,?*$@£&\-_\/()[\]{}#|])[A-Za-z0-9:!;,?*$@£&\-_\/()[\]{}#|]{6,}$/';

        return (bool) preg_match($pattern, $password);
    }
}
